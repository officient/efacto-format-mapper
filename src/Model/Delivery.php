<?php

namespace Officient\EfactoMapper\Model;

class Delivery
{
    /** @var string|null */
    protected ?string $actualDeliveryDate;
    /** @var string|null */
    protected ?string $streetName;
    /** @var string|null */
    protected ?string $cityName;
    /** @var string|null */
    protected ?string $postalZone;
    /** @var string|null */
    protected ?string $countryCode;

    /**
     * @param string|null $actualDeliveryDate
     * @param string|null $streetName
     * @param string|null $cityName
     * @param string|null $postalZone
     * @param string|null $countryCode
     */
    public function __construct(?string $actualDeliveryDate, ?string $streetName, ?string $cityName, ?string $postalZone, ?string $countryCode)
    {
        $this->actualDeliveryDate = $actualDeliveryDate;
        $this->streetName = $streetName;
        $this->cityName = $cityName;
        $this->postalZone = $postalZone;
        $this->countryCode = $countryCode;
    }

    /**
     * @return string|null
     */
    public function getActualDeliveryDate(): ?string
    {
        return $this->actualDeliveryDate;
    }

    /**
     * @return string|null
     */
    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    /**
     * @return string|null
     */
    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    /**
     * @return string|null
     */
    public function getPostalZone(): ?string
    {
        return $this->postalZone;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }
}