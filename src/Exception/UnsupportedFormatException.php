<?php

namespace Officient\EfactoMapper\Exception;

use Throwable;

class UnsupportedFormatException extends \Exception
{
    public function __construct($message = "The format is not supported", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}