<?php

namespace Officient\EfactoMapper\Model;

class Supplier extends Party
{
    /** @var string|null */
    protected ?string $customerAssignedAccountId;

    /**
     * @param string|null $customerAssignedAccountId
     * @param string|null $endpointSchemeId
     * @param string|null $endpointId
     * @param string|null $partyIdentificationSchemeId
     * @param string|null $partyIdentificationId
     * @param string|null $name
     * @param string|null $streetName
     * @param string|null $cityName
     * @param string|null $postalZone
     * @param string|null $countryCode
     * @param string|null $partyTaxSchemeCompanyId
     * @param string|null $partyLegalEntityCompanySchemeId
     * @param string|null $partyLegalEntityCompanyId
     * @param string|null $contactName
     * @param string|null $contactTelephone
     * @param string|null $contactElectronicMail
     */
    public function __construct(?string $customerAssignedAccountId, ?string $endpointSchemeId, ?string $endpointId, ?string $partyIdentificationSchemeId, ?string $partyIdentificationId, ?string $name, ?string $streetName, ?string $cityName, ?string $postalZone, ?string $countryCode, ?string $partyTaxSchemeCompanyId, ?string $partyLegalEntityCompanySchemeId, ?string $partyLegalEntityCompanyId, ?string $contactName, ?string $contactTelephone, ?string $contactElectronicMail)
    {
        parent::__construct($endpointSchemeId, $endpointId, $partyIdentificationSchemeId, $partyIdentificationId, $name, $streetName, $cityName, $postalZone, $countryCode, $partyTaxSchemeCompanyId, $partyLegalEntityCompanySchemeId, $partyLegalEntityCompanyId, $contactName, $contactTelephone, $contactElectronicMail);
        $this->customerAssignedAccountId = $customerAssignedAccountId;
    }

    /**
     * @return string|null
     */
    public function getCustomerAssignedAccountId(): ?string
    {
        return $this->customerAssignedAccountId;
    }
}