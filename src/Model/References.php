<?php

namespace Officient\EfactoMapper\Model;

class References
{
    /** @var string|null */
    protected ?string $accountingCost;
    /** @var string|null */
    protected ?string $buyerReference;
    /** @var string|null */
    protected ?string $invoicePeriodStartDate;
    /** @var string|null */
    protected ?string $invoicePeriodEndDate;
    /** @var string|null */
    protected ?string $orderReference;
    /** @var string|null */
    protected ?string $contractDocumentReference;
    /** @var string|null */
    protected ?string $projectReference;
    /** @var string|null */
    protected ?string $billingReferenceID;

    /**
     * @param string|null $accountingCost
     * @param string|null $buyerReference
     * @param string|null $invoicePeriodStartDate
     * @param string|null $invoicePeriodEndDate
     * @param string|null $orderReference
     * @param string|null $contractDocumentReference
     * @param string|null $projectReference
     * @param string|null $billingReferenceID
     */
    public function __construct(?string $accountingCost, ?string $buyerReference, ?string $invoicePeriodStartDate, ?string $invoicePeriodEndDate, ?string $orderReference, ?string $contractDocumentReference, ?string $projectReference, ?string $billingReferenceID)
    {
        $this->accountingCost = $accountingCost;
        $this->buyerReference = $buyerReference;
        $this->invoicePeriodStartDate = $invoicePeriodStartDate;
        $this->invoicePeriodEndDate = $invoicePeriodEndDate;
        $this->orderReference = $orderReference;
        $this->contractDocumentReference = $contractDocumentReference;
        $this->projectReference = $projectReference;
        $this->billingReferenceID = $billingReferenceID;
    }

    /**
     * @return string|null
     */
    public function getAccountingCost(): ?string
    {
        return $this->accountingCost;
    }

    /**
     * @return string|null
     */
    public function getBuyerReference(): ?string
    {
        return $this->buyerReference;
    }

    /**
     * @return string|null
     */
    public function getInvoicePeriodStartDate(): ?string
    {
        return $this->invoicePeriodStartDate;
    }

    /**
     * @return string|null
     */
    public function getInvoicePeriodEndDate(): ?string
    {
        return $this->invoicePeriodEndDate;
    }

    /**
     * @return string|null
     */
    public function getOrderReference(): ?string
    {
        return $this->orderReference;
    }

    /**
     * @return string|null
     */
    public function getContractDocumentReference(): ?string
    {
        return $this->contractDocumentReference;
    }

    /**
     * @return string|null
     */
    public function getProjectReference(): ?string
    {
        return $this->projectReference;
    }

    /**
     * @return string|null
     */
    public function getBillingReferenceID(): ?string
    {
        return $this->billingReferenceID;
    }
}