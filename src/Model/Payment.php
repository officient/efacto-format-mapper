<?php

namespace Officient\EfactoMapper\Model;

class Payment
{
    /** @var string|null */
    protected ?string $paymentMeansCode;
    /** @var string|null */
    protected ?string $paymentId;
    /** @var string|null */
    protected ?string $payeeFinancialAccountId;
    /** @var string|null */
    protected ?string $payeeFinancialAccountName;
    /** @var string|null */
    protected ?string $financialInstitutionBranchId;
    /** @var PaymentMandate|null */
    protected ?PaymentMandate $paymentMandate;

    /**
     * @param string|null $paymentMeansCode
     * @param string|null $paymentId
     * @param string|null $payeeFinancialAccountId
     * @param string|null $payeeFinancialAccountName
     * @param string|null $financialInstitutionBranchId
     * @param PaymentMandate|null $paymentMandate
     */
    public function __construct(?string $paymentMeansCode, ?string $paymentId, ?string $payeeFinancialAccountId, ?string $payeeFinancialAccountName, ?string $financialInstitutionBranchId, ?PaymentMandate $paymentMandate)
    {
        $this->paymentMeansCode = $paymentMeansCode;
        $this->paymentId = $paymentId;
        $this->payeeFinancialAccountId = $payeeFinancialAccountId;
        $this->payeeFinancialAccountName = $payeeFinancialAccountName;
        $this->financialInstitutionBranchId = $financialInstitutionBranchId;
        $this->paymentMandate = $paymentMandate;
    }

    /**
     * @return string|null
     */
    public function getPaymentMeansCode(): ?string
    {
        return $this->paymentMeansCode;
    }

    /**
     * @return string|null
     */
    public function getPaymentId(): ?string
    {
        return $this->paymentId;
    }

    /**
     * @return string|null
     */
    public function getPayeeFinancialAccountId(): ?string
    {
        return $this->payeeFinancialAccountId;
    }

    /**
     * @return string|null
     */
    public function getPayeeFinancialAccountName(): ?string
    {
        return $this->payeeFinancialAccountName;
    }

    /**
     * @return string|null
     */
    public function getFinancialInstitutionBranchId(): ?string
    {
        return $this->financialInstitutionBranchId;
    }

    /**
     * @return PaymentMandate|null
     */
    public function getPaymentMandate(): ?PaymentMandate
    {
        return $this->paymentMandate;
    }
}