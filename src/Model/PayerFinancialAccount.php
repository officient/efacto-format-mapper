<?php

namespace Officient\EfactoMapper\Model;

class PayerFinancialAccount
{
    /** @var string|null */
    protected ?string $id;

    /**
     * @param string|null $id
     */
    public function __construct(?string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getID(): ?string
    {
        return $this->id;
    }
}