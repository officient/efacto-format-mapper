<?php

namespace Officient\EfactoMapper\Model;

class Line
{
    /** @var string|null */
    protected ?string $id;
    /** @var string|null */
    protected ?string $note;
    /** @var string|null */
    protected ?string $quantity;
    /** @var string|null */
    protected ?string $unitCode;
    /** @var string|null */
    protected ?string $lineExtensionAmount;
    /** @var string|null */
    protected ?string $accountingCost;
    /** @var string|null */
    protected ?string $invoicePeriodStartDate;
    /** @var string|null */
    protected ?string $invoicePeriodEndDate;
    /** @var string|null */
    protected ?string $orderLineReferenceLineId;
    /** @var string|null */
    protected ?string $buyersItemIdentificationId;
    /** @var string|null */
    protected ?string $sellersItemIdentificationId;
    /** @var string|null */
    protected ?string $standardItemIdentificationId;
    /** @var string|null */
    protected ?string $description;
    /** @var string|null */
    protected ?string $name;
    /** @var string|null */
    protected ?string $taxCategoryId;
    /** @var string|null */
    protected ?string $taxCategoryPercent;
    /** @var LineAllowanceAndCharge[] */
    protected array $allowanceAndCharges;
    /** @var string|null */
    protected ?string $price;
    /** @var string|null */
    protected ?string $baseQuantity;
    /** @var string|null */
    protected ?string $baseQuantityUnitCode;
    /** @var string|null */
    protected ?string $priceAllowanceChargeIndicator;
    /** @var string|null */
    protected ?string $priceAllowanceChargeAmount;
    /** @var string|null */
    protected ?string $priceAllowanceChargeBaseAmount;

    /**
     * @param string|null $id
     * @param string|null $note
     * @param string|null $quantity
     * @param string|null $unitCode
     * @param string|null $lineExtensionAmount
     * @param string|null $accountingCost
     * @param string|null $invoicePeriodStartDate
     * @param string|null $invoicePeriodEndDate
     * @param string|null $orderLineReferenceLineId
     * @param string|null $buyersItemIdentificationId
     * @param string|null $sellersItemIdentificationId
     * @param string|null $standardItemIdentificationId
     * @param string|null $description
     * @param string|null $name
     * @param string|null $taxCategoryId
     * @param string|null $taxCategoryPercent
     * @param LineAllowanceAndCharge[] $allowanceAndCharges
     * @param string|null $price
     * @param string|null $baseQuantity
     * @param string|null $baseQuantityUnitCode
     * @param string|null $priceAllowanceChargeIndicator
     * @param string|null $priceAllowanceChargeAmount
     * @param string|null $priceAllowanceChargeBaseAmount
     */
    public function __construct(?string $id, ?string $note, ?string $quantity, ?string $unitCode, ?string $lineExtensionAmount, ?string $accountingCost, ?string $invoicePeriodStartDate, ?string $invoicePeriodEndDate, ?string $orderLineReferenceLineId, ?string $buyersItemIdentificationId, ?string $sellersItemIdentificationId, ?string $standardItemIdentificationId, ?string $description, ?string $name, ?string $taxCategoryId, ?string $taxCategoryPercent, array $allowanceAndCharges, ?string $price, ?string $baseQuantity, ?string $baseQuantityUnitCode, ?string $priceAllowanceChargeIndicator, ?string $priceAllowanceChargeAmount, ?string $priceAllowanceChargeBaseAmount)
    {
        $this->id = $id;
        $this->note = $note;
        $this->quantity = $quantity;
        $this->unitCode = $unitCode;
        $this->lineExtensionAmount = $lineExtensionAmount;
        $this->accountingCost = $accountingCost;
        $this->invoicePeriodStartDate = $invoicePeriodStartDate;
        $this->invoicePeriodEndDate = $invoicePeriodEndDate;
        $this->orderLineReferenceLineId = $orderLineReferenceLineId;
        $this->buyersItemIdentificationId = $buyersItemIdentificationId;
        $this->sellersItemIdentificationId = $sellersItemIdentificationId;
        $this->standardItemIdentificationId = $standardItemIdentificationId;
        $this->description = $description;
        $this->name = $name;
        $this->taxCategoryId = $taxCategoryId;
        $this->taxCategoryPercent = $taxCategoryPercent;
        $this->allowanceAndCharges = $allowanceAndCharges;
        $this->price = $price;
        $this->baseQuantity = $baseQuantity;
        $this->baseQuantityUnitCode = $baseQuantityUnitCode;
        $this->priceAllowanceChargeIndicator = $priceAllowanceChargeIndicator;
        $this->priceAllowanceChargeAmount = $priceAllowanceChargeAmount;
        $this->priceAllowanceChargeBaseAmount = $priceAllowanceChargeBaseAmount;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @return string|null
     */
    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    /**
     * @return string|null
     */
    public function getUnitCode(): ?string
    {
        return $this->unitCode;
    }

    /**
     * @return string|null
     */
    public function getLineExtensionAmount(): ?string
    {
        return $this->lineExtensionAmount;
    }

    /**
     * @return string|null
     */
    public function getAccountingCost(): ?string
    {
        return $this->accountingCost;
    }

    /**
     * @return string|null
     */
    public function getInvoicePeriodStartDate(): ?string
    {
        return $this->invoicePeriodStartDate;
    }

    /**
     * @return string|null
     */
    public function getInvoicePeriodEndDate(): ?string
    {
        return $this->invoicePeriodEndDate;
    }

    /**
     * @return string|null
     */
    public function getOrderLineReferenceLineId(): ?string
    {
        return $this->orderLineReferenceLineId;
    }

    /**
     * @return string|null
     */
    public function getBuyersItemIdentificationId(): ?string
    {
        return $this->buyersItemIdentificationId;
    }

    /**
     * @return string|null
     */
    public function getSellersItemIdentificationId(): ?string
    {
        return $this->sellersItemIdentificationId;
    }

    /**
     * @return string|null
     */
    public function getStandardItemIdentificationId(): ?string
    {
        return $this->standardItemIdentificationId;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getTaxCategoryId(): ?string
    {
        return $this->taxCategoryId;
    }

    /**
     * @return string|null
     */
    public function getTaxCategoryPercent(): ?string
    {
        return $this->taxCategoryPercent;
    }

    /**
     * @return LineAllowanceAndCharge[]
     */
    public function getAllowanceAndCharges(): array
    {
        return $this->allowanceAndCharges;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @return string|null
     */
    public function getBaseQuantity(): ?string
    {
        return $this->baseQuantity;
    }

    /**
     * @return string|null
     */
    public function getBaseQuantityUnitCode(): ?string
    {
        return $this->baseQuantityUnitCode;
    }

    /**
     * @return string|null
     */
    public function getPriceAllowanceChargeIndicator(): ?string
    {
        return $this->priceAllowanceChargeIndicator;
    }

    /**
     * @return string|null
     */
    public function getPriceAllowanceChargeAmount(): ?string
    {
        return $this->priceAllowanceChargeAmount;
    }

    /**
     * @return string|null
     */
    public function getPriceAllowanceChargeBaseAmount(): ?string
    {
        return $this->priceAllowanceChargeBaseAmount;
    }
}