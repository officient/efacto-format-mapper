<?php

namespace Officient\EfactoMapper;

use DOMDocument;
use Officient\EfactoMapper\Model\Document;
use DOMNode;

class Peppol3Exporter extends AbstractExporter
{
    const namespaces = [
        'cac' => 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2',
        'cbc' => 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2',
        'cec' => 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2'
    ];

    const customizationID = 'urn:cen.eu:en16931:2017#compliant#urn:fdc:peppol.eu:2017:poacc:billing:3.0';
    const profileID = 'urn:fdc:peppol.eu:2017:poacc:billing:01:1.0';

    public function export(Document $document): DOMDocument
    {
        // Init dom
        $dom = $this->initDOMDocument();

        // Map
        $rootNode = null;
        if($document->getDocType() === "invoice") {
            $rootNode = $dom->createElement('Invoice');
            $rootNode->setAttribute('xmlns', 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2');
        } else if(in_array($document->getDocType(), ["creditnote", "credit_note"])) {
            $rootNode = $dom->createElement('CreditNote');
            $rootNode->setAttribute('xmlns', 'urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2');
        }

        if($rootNode) {
            // Root attributes
            foreach (self::namespaces as $key => $value) {
                $rootNode->setAttribute('xmlns:'.$key, $value);
            }

            // CustomizationID
            $cNode = $dom->createElement('cbc:CustomizationID', self::customizationID);
            $rootNode->appendChild($cNode);

            // ProfileID
            $pNode = $dom->createElement('cbc:ProfileID', self::profileID);
            $rootNode->appendChild($pNode);

            // ID
            if(!empty($document->getDocNumber())) {
                $cNode = $dom->createElement('cbc:ID', $this->xmlEscape($document->getDocNumber()));
                $rootNode->appendChild($cNode);
            }

            // IssueDate
            if(!empty($document->getIssueDate())) {
                $IDNode = $dom->createElement('cbc:IssueDate', $this->xmlEscape($document->getIssueDate()));
                $rootNode->appendChild($IDNode);
            }

            // DueDate
            if ($document->getDocType() === "invoice" && !empty($document->getDueDate())) {
                $cNode = $dom->createElement('cbc:DueDate', $this->xmlEscape($document->getDueDate()));
                $rootNode->appendChild($cNode);
            }

            // InvoiceTypeCode / CreditNoteTypeCode
            if (!empty($document->getInvoiceTypeCode()) && in_array($document->getInvoiceTypeCode(), ['81','83','381','396','532'])) {
                $cNode = $dom->createElement('cbc:CreditNoteTypeCode', $document->getInvoiceTypeCode());
                $rootNode->appendChild($cNode);
            } else if(!empty($document->getInvoiceTypeCode())) {
                $cNode = $dom->createElement('cbc:InvoiceTypeCode', $document->getInvoiceTypeCode());
                $rootNode->appendChild($cNode);
            } else if($document->getDocType() === "invoice") {
                $cNode = $dom->createElement('cbc:InvoiceTypeCode', '380');
                $rootNode->appendChild($cNode);
            } else if(in_array($document->getDocType(), ["creditnote", "credit_note"])) {
                $cNode = $dom->createElement('cbc:CreditNoteTypeCode', '381');
                $rootNode->appendChild($cNode);
            }

            // Note
            if(!empty($document->getNote())) {
                $cNode = $dom->createElement('cbc:Note', $this->xmlEscape($document->getNote()));
                $rootNode->appendChild($cNode);
            }

            // DocumentCurrencyCode
            if(!empty($document->getCurrencyCode())) {
                $cNode = $dom->createElement('cbc:DocumentCurrencyCode', $this->xmlEscape($document->getCurrencyCode()));
                $rootNode->appendChild($cNode);
            }

            // AccountingCost
            if(!empty($document->getReferences()->getAccountingCost())) {
                $cNode = $dom->createElement('cbc:AccountingCost', $this->xmlEscape($document->getReferences()->getAccountingCost()));
                $rootNode->appendChild($cNode);
            }

            // BuyerReference
            if(!empty($document->getReferences()->getBuyerReference())) {
                $cNode = $dom->createElement('cbc:BuyerReference', $this->xmlEscape($document->getReferences()->getBuyerReference()));
                $rootNode->appendChild($cNode);
            }

            // InvoicePeriod
            if(!empty($document->getReferences()->getInvoicePeriodStartDate()) && !empty($document->getReferences()->getInvoicePeriodEndDate())) {
                $cNode = $dom->createElement('cac:InvoicePeriod');
                $cNode1 = $dom->createElement('cbc:StartDate', $this->xmlEscape($document->getReferences()->getInvoicePeriodStartDate()));
                $cNode2 = $dom->createElement('cbc:EndDate', $this->xmlEscape($document->getReferences()->getInvoicePeriodEndDate()));
                $cNode->appendChild($cNode1);
                $cNode->appendChild($cNode2);
                $rootNode->appendChild($cNode);
            }

            // OrderReference
            if(!empty($document->getReferences()->getOrderReference())) {
                $cNode = $dom->createElement('cac:OrderReference');
                $cNode1 = $dom->createElement('cbc:ID', $this->xmlEscape($document->getReferences()->getOrderReference()));
                $cNode->appendChild($cNode1);
                $rootNode->appendChild($cNode);
            }

            // ContractDocumentReference
            if(!empty($document->getReferences()->getContractDocumentReference())) {
                $cNode = $dom->createElement('cac:ContractDocumentReference');
                $cNode1 = $dom->createElement('cbc:ID', $this->xmlEscape($document->getReferences()->getContractDocumentReference()));
                $cNode->appendChild($cNode1);
                $rootNode->appendChild($cNode);
            }

            // AdditionDocumentReference(s)
            $cNodes = $this->getAdditionalDocumentReferenceNodes($document, $dom);
            foreach ($cNodes as $cNode) {
                $rootNode->appendChild($cNode);
            }

            // ProjectReference
            if(!empty($document->getReferences()->getProjectReference())) {
                $cNode = $dom->createElement('cac:ProjectReference');
                $cNode1 = $dom->createElement('cbc:ID', $this->xmlEscape($document->getReferences()->getProjectReference()));
                $cNode->appendChild($cNode1);
                $rootNode->appendChild($cNode);
            }

            // BillingReference
            if(!empty($document->getReferences()->getBillingReferenceID())) {
                $cNode = $dom->createElement('cac:BillingReference');
                $cNode1 = $dom->createElement('cac:InvoiceDocumentReference');
                $cNode2 = $dom->createElement('cbc:ID', $this->xmlEscape($document->getReferences()->getBillingReferenceID()));
                $cNode1->appendChild($cNode2);
                $cNode->appendChild($cNode1);
                $rootNode->appendChild($cNode);
            }

            // AccountingSupplierParty
            if($cNode = $this->getAccountingSupplierPartyNode($document, $dom)) {
                $rootNode->appendChild($cNode);
            }

            // AccountingCustomerParty
            if($cNode = $this->getAccountingCustomerPartyNode($document, $dom)) {
                $rootNode->appendChild($cNode);
            }

            // Delivery
            if($cNode = $this->getDeliveryNode($document, $dom)) {
                $rootNode->appendChild($cNode);
            }

            // PaymentMeans
            $paymentMeansNodes = $this->getPaymentMeansNodes($document, $dom);
            foreach ($paymentMeansNodes as $paymentMeansNode) {
                $rootNode->appendChild($paymentMeansNode);
            }

            // PaymentTerms
            if(!empty($document->getPaymentTermsNote())) {
                $cNode = $dom->createElement('cac:PaymentTerms');
                $cNode1 = $dom->createElement('cbc:Note', $this->xmlEscape($document->getPaymentTermsNote()));
                $cNode->appendChild($cNode1);
                $rootNode->appendChild($cNode);
            }

            // AllowanceCharge
            $allowanceChargeNodes = $this->getAllowanceChangeNodes($document, $dom);
            foreach ($allowanceChargeNodes as $allowanceChargeNode) {
                $rootNode->appendChild($allowanceChargeNode);
            }

            // TaxTotal
            if($cNode = $this->getTaxTotalNode($document, $dom)) {
                $rootNode->appendChild($cNode);
            }

            // LegalMonetaryTotal
            if($cNode = $this->getLegalMonetaryTotalNode($document, $dom)) {
                $rootNode->appendChild($cNode);
            }

            // InvoiceLines / CreditNoteLines
            $invoiceLineAndCreditNoteLineNodes = $this->getInvoiceLineAndCreditNoteLineNodes($document, $dom);
            foreach ($invoiceLineAndCreditNoteLineNodes as $lineNode) {
                $rootNode->appendChild($lineNode);
            }

            $dom->appendChild($rootNode);
        }

        return $dom;
    }

    private function getAdditionalDocumentReferenceNodes(Document $document, DOMDocument $dom): array
    {
        $attachments = $document->getAttachments();
        $nodes = array();
        foreach ($attachments as $attachment) {
            if(
                !empty($attachment->getMimeCode())
                && !empty($attachment->getId())
                && !empty($attachment->getFilename())
                && !empty($attachment->getDocument())
            ) {
                if($additionDocumentReferenceNode = $dom->createElement('cac:AdditionalDocumentReference')) {
                    if($idNode = $dom->createElement('cbc:ID', $attachment->getId())) {
                        if($attachmentNode = $dom->createElement('cac:Attachment')) {
                            if($embeddedDocumentBinaryObjectNode = $dom->createElement('cbc:EmbeddedDocumentBinaryObject', $attachment->getDocument())) {
                                $embeddedDocumentBinaryObjectNode->setAttribute('mimeCode', $attachment->getMimeCode());
                                $embeddedDocumentBinaryObjectNode->setAttribute('filename', $attachment->getFilename());

                                $additionDocumentReferenceNode->appendChild($idNode);
                                $attachmentNode->appendChild($embeddedDocumentBinaryObjectNode);
                                $additionDocumentReferenceNode->appendChild($attachmentNode);
                                $nodes[] = $additionDocumentReferenceNode;
                            }
                        }
                    }
                }
            }
        }
        return $nodes;
    }

    private function getAccountingSupplierPartyNode(Document $document, DOMDocument $dom): ?DOMNode
    {
        $supplier = $document->getSupplier();
        if($partyNode = $this->getPartyNode(
            $supplier->getEndpointSchemeId() ?? null,
            $supplier->getEndpointId() ?? null,
            $supplier->getPartyIdentificationSchemeId() ?? null,
            $supplier->getPartyIdentificationId() ?? null,
            $supplier->getName() ?? null,
            $supplier->getStreetName() ?? null,
            $supplier->getCityName() ?? null,
            $supplier->getPostalZone() ?? null,
            $supplier->getCountryCode() ?? null,
            $supplier->getPartyTaxSchemeCompanyId() ?? null,
            $supplier->getPartyLegalEntityCompanySchemeId() ?? null,
            $supplier->getPartyLegalEntityCompanyId() ?? null,
            $supplier->getContactName() ?? null,
            $supplier->getContactTelephone() ?? null,
            $supplier->getContactElectronicMail() ?? null,
            $dom
        )) {
            $accountingSupplierPartyNode = $dom->createElement('cac:AccountingSupplierParty');
            if(!empty($document->getSupplier()->getCustomerAssignedAccountId())) {
                $customerAssignedAccountIdNode = $dom->createElement('cbc:CustomerAssignedAccountID', $this->xmlEscape($document->getSupplier()->getCustomerAssignedAccountId()));
                $accountingSupplierPartyNode->appendChild($customerAssignedAccountIdNode);
            }
            $accountingSupplierPartyNode->appendChild($partyNode);
            return $accountingSupplierPartyNode;
        } else {
            return null;
        }
    }

    private function getAccountingCustomerPartyNode(Document $document, DOMDocument $dom): ?DOMNode
    {
        $customer = $document->getCustomer();
        if($partyNode = $this->getPartyNode(
            $customer->getEndpointSchemeId() ?? null,
            $customer->getEndpointId() ?? null,
            $customer->getPartyIdentificationSchemeId() ?? null,
            $customer->getPartyIdentificationId() ?? null,
            $customer->getName() ?? null,
            $customer->getStreetName() ?? null,
            $customer->getCityName() ?? null,
            $customer->getPostalZone() ?? null,
            $customer->getCountryCode() ?? null,
            $customer->getPartyTaxSchemeCompanyId() ?? null,
            $customer->getPartyLegalEntityCompanySchemeId() ?? null,
            $customer->getPartyLegalEntityCompanyId() ?? null,
            $customer->getContactName() ?? null,
            $customer->getContactTelephone() ?? null,
            $customer->getContactElectronicMail() ?? null,
            $dom
        )) {
            $accountingCustomerPartyNode = $dom->createElement('cac:AccountingCustomerParty');
            $accountingCustomerPartyNode->appendChild($partyNode);
            return $accountingCustomerPartyNode;
        } else {
            return null;
        }
    }

    private function getPartyNode(
        ?string $endpointSchemeId,
        ?string $endpointId,
        ?string $partyIdentificationSchemeId,
        ?string $partyIdentificationId,
        ?string $name,
        ?string $streetName,
        ?string $cityName,
        ?string $postalZone,
        ?string $countryCode,
        ?string $partyTaxSchemeCompanyId,
        ?string $partyLegalEntityCompanySchemeId,
        ?string $partyLegalEntityCompanyId,
        ?string $contactName,
        ?string $contactTelephone,
        ?string $contactElectronicMail,
        DOMDocument $dom
    ): ?DOMNode
    {
        $partyNode = $dom->createElement('cac:Party');

        // EndpointID
        if(!empty($endpointId) && !empty($endpointSchemeId)) {
            $endpointIdNode = $dom->createElement('cbc:EndpointID', $this->xmlEscape($endpointId));
            $endpointIdNode->setAttribute('schemeID', $this->xmlEscape($endpointSchemeId));
            $partyNode->appendChild($endpointIdNode);
        } else if(!empty($partyIdentificationId) && !empty($partyIdentificationSchemeId)) {
            $endpointIdNode = $dom->createElement('cbc:EndpointID', $this->xmlEscape($partyIdentificationId));
            $endpointIdNode->setAttribute('schemeID', $this->xmlEscape($partyIdentificationSchemeId));
            $partyNode->appendChild($endpointIdNode);
        }

        // PartyIdentification
        if(!empty($partyIdentificationId)) {
            $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($partyIdentificationId));
            if(!empty($partyIdentificationSchemeId)) {
                $idNode->setAttribute('schemeID', $this->xmlEscape($partyIdentificationSchemeId));
            }
            $partyIdentificationNode = $dom->createElement('cac:PartyIdentification');
            $partyIdentificationNode->appendChild($idNode);
            $partyNode->appendChild($partyIdentificationNode);
        }

        // PartyName
        if(!empty($name)) {
            $nameNode = $dom->createElement('cbc:Name', $this->xmlEscape($name));
            $partyNameNode = $dom->createElement('cac:PartyName');
            $partyNameNode->appendChild($nameNode);
            $partyNode->appendChild($partyNameNode);
        }

        // PostalAddress
        $postalAddressNode = $dom->createElement('cac:PostalAddress');

        if(!empty($streetName)) {
            $streetNameNode = $dom->createElement('cbc:StreetName', $this->xmlEscape($streetName));
            $postalAddressNode->appendChild($streetNameNode);
        }
        if(!empty($cityName)) {
            $cityNameNode = $dom->createElement('cbc:CityName', $this->xmlEscape($cityName));
            $postalAddressNode->appendChild($cityNameNode);
        }
        if(!empty($postalZone)) {
            $postalZoneNode = $dom->createElement('cbc:PostalZone', $this->xmlEscape($postalZone));
            $postalAddressNode->appendChild($postalZoneNode);
        }
        if(!empty($countryCode)) {
            $identificationCodeNode = $dom->createElement('cbc:IdentificationCode', $this->xmlEscape($countryCode));
            $countryNode = $dom->createElement('cac:Country');
            $countryNode->appendChild($identificationCodeNode);
            $postalAddressNode->appendChild($countryNode);
        }
        if($postalAddressNode->childNodes->count() > 0) {
            $partyNode->appendChild($postalAddressNode);
        }

        // PartyTaxScheme
        if(!empty($partyTaxSchemeCompanyId)) {
            $companyIdNode = $dom->createElement('cbc:CompanyID', $this->xmlEscape($partyTaxSchemeCompanyId));
            $taxSchemeNode = $dom->createElement('cac:TaxScheme');
            $idNode = $dom->createElement('cbc:ID', 'VAT');
            $taxSchemeNode->appendChild($idNode);
            $partyTaxSchemeNode = $dom->createElement('cac:PartyTaxScheme');
            $partyTaxSchemeNode->appendChild($companyIdNode);
            $partyTaxSchemeNode->appendChild($taxSchemeNode);
            $partyNode->appendChild($partyTaxSchemeNode);
        }

        if($partyIdentificationSchemeId === "0192" || $countryCode === "NO") {
            $companyIdNode = $dom->createElement('cbc:CompanyID', $this->xmlEscape("Foretaksregisteret"));
            $taxSchemeNode = $dom->createElement('cac:TaxScheme');
            $idNode = $dom->createElement('cbc:ID', 'TAX');
            $taxSchemeNode->appendChild($idNode);
            $partyTaxSchemeNode = $dom->createElement('cac:PartyTaxScheme');
            $partyTaxSchemeNode->appendChild($companyIdNode);
            $partyTaxSchemeNode->appendChild($taxSchemeNode);
            $partyNode->appendChild($partyTaxSchemeNode);
        }

        // PartyLegalEntity
        $partyLegalEntityNode = $dom->createElement('cac:PartyLegalEntity');
        if(!empty($name)) {
            $registrationNameNode = $dom->createElement('cbc:RegistrationName', $this->xmlEscape($name));
            $partyLegalEntityNode->appendChild($registrationNameNode);
        }
        if(!empty($partyLegalEntityCompanyId)) {
            $companyIdNode = $dom->createElement('cbc:CompanyID', $this->xmlEscape($partyLegalEntityCompanyId));
            if(!empty($partyLegalEntityCompanySchemeId)) {
                $companyIdNode->setAttribute('schemeID', $this->xmlEscape($partyLegalEntityCompanySchemeId));
            }
            $partyLegalEntityNode->appendChild($companyIdNode);
        }
        if($partyLegalEntityNode->childNodes->count() > 0) {
            $partyNode->appendChild($partyLegalEntityNode);
        }

        // Contact
        $contactNode = $dom->createElement('cac:Contact');
        if(!empty($contactName)) {
            $nameNode = $dom->createElement('cbc:Name', $this->xmlEscape($contactName));
            $contactNode->appendChild($nameNode);
        }
        if(!empty($contactTelephone)) {
            $telephoneNode = $dom->createElement('cbc:Telephone', $this->xmlEscape($contactTelephone));
            $contactNode->appendChild($telephoneNode);
        }
        if(!empty($contactElectronicMail)) {
            $electronicMail = $dom->createElement('cbc:ElectronicMail', $this->xmlEscape($contactElectronicMail));
            $contactNode->appendChild($electronicMail);
        }
        if($contactNode->childNodes->count() > 0) {
            $partyNode->appendChild($contactNode);
        }

        return $partyNode->childNodes->count() > 0 ? $partyNode : null;
    }

    private function getDeliveryNode(Document $document, DOMDocument $dom): ?DOMNode
    {
        $delivery = $document->getDelivery();

        $deliveryNode = $dom->createElement('cac:Delivery');

        // ActualDeliveryDate
        if(!empty($delivery->getActualDeliveryDate())) {
            $actualDeliveryDateNode = $dom->createElement('cbc:ActualDeliveryDate', $this->xmlEscape($delivery->getActualDeliveryDate()));
            $deliveryNode->appendChild($actualDeliveryDateNode);
        }

        // DeliveryLocation
        $deliveryLocationNode = $dom->createElement('cac:DeliveryLocation');
        $addressNode = $dom->createElement('cac:Address');
        if(!empty($delivery->getStreetName())) {
            $streetNameNode = $dom->createElement('cbc:StreetName', $this->xmlEscape($delivery->getStreetName()));
            $addressNode->appendChild($streetNameNode);
        }
        if(!empty($delivery->getCityName())) {
            $cityNameNode = $dom->createElement('cbc:CityName', $this->xmlEscape($delivery->getCityName()));
            $addressNode->appendChild($cityNameNode);
        }
        if(!empty($delivery->getPostalZone())) {
            $postalZoneNode = $dom->createElement('cbc:PostalZone', $this->xmlEscape($delivery->getPostalZone()));
            $addressNode->appendChild($postalZoneNode);
        }
        if(!empty($delivery->getCountryCode())) {
            $identificationCodeNode = $dom->createElement('cbc:IdentificationCode', $this->xmlEscape($delivery->getCountryCode()));
            $countryNode = $dom->createElement('cac:Country');
            $countryNode->appendChild($identificationCodeNode);
            $addressNode->appendChild($countryNode);
        }
        if($addressNode->childNodes->count() > 0) {
            $deliveryLocationNode->appendChild($addressNode);
            $deliveryNode->appendChild($deliveryLocationNode);
        }

        return $deliveryNode->childNodes->count() > 0 ? $deliveryNode : null;
    }

    private function getPaymentMeansNodes(Document $document, DOMDocument $dom): array
    {
        $payments = $document->getPayments();

        $nodes = array();
        foreach ($payments as $payment) {
            $paymentMeansNode = $dom->createElement('cac:PaymentMeans');

            // PaymentMeansCode
            if(!empty($payment->getPaymentMeansCode())) {
                $paymentMeansCodeNode = $dom->createElement('cbc:PaymentMeansCode', $this->xmlEscape($payment->getPaymentMeansCode()));
                $paymentMeansNode->appendChild($paymentMeansCodeNode);
            }

            // PaymentID
            if(!empty($payment->getPaymentId())) {
                $paymentIdNode = $dom->createElement('cbc:PaymentID', $this->xmlEscape($payment->getPaymentId()));
                $paymentMeansNode->appendChild($paymentIdNode);
            }

            // PayeeFinancialAccount
            $payeeFinancialAccountNode = $dom->createElement('cac:PayeeFinancialAccount');

            if(!empty($payment->getPayeeFinancialAccountId())) {
                $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($payment->getPayeeFinancialAccountId()));
                $payeeFinancialAccountNode->appendChild($idNode);
            }

            if(!empty($payment->getPayeeFinancialAccountName())) {
                $nameNode = $dom->createElement('cbc:Name', $this->xmlEscape($payment->getPayeeFinancialAccountName()));
                $payeeFinancialAccountNode->appendChild($nameNode);
            }

            if(!empty($payment->getFinancialInstitutionBranchId())) {
                $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($payment->getFinancialInstitutionBranchId()));
                $financialInstitutionBranchNode = $dom->createElement('cac:FinancialInstitutionBranch');
                $financialInstitutionBranchNode->appendChild($idNode);
                $payeeFinancialAccountNode->appendChild($financialInstitutionBranchNode);
            }

            if($payeeFinancialAccountNode->childNodes->count() > 0) {
                $paymentMeansNode->appendChild($payeeFinancialAccountNode);
            }

            //PaymentMandate
            if(!empty($payment->getPaymentMandate())) {
                $paymentMandateNode = $dom->createElement('cac:PaymentMandate');
                if (!empty($payment->getPaymentMandate()->getID())) {
                    $paymentMandateIDNode = $dom->createElement('cbc:ID', $this->xmlEscape($payment->getPaymentMandate()->getID()));
                    $paymentMandateNode->appendChild($paymentMandateIDNode);
                }
                if ($payment->getPaymentMandate()->getPayerFinancialAccount()) {
                    $payerFinancialAccount = $payment->getPaymentMandate()->getPayerFinancialAccount();
                    $payerFinancialAccountNode = $dom->createElement('cac:PayerFinancialAccount');
                    if (!empty($payerFinancialAccount->getID())) {
                        $payerFinancialAccountIDNode = $dom->createElement('cbc:ID', $this->xmlEscape($payerFinancialAccount->getID()));
                        $payerFinancialAccountNode->appendChild($payerFinancialAccountIDNode);
                    }
                    $paymentMandateNode->appendChild($payerFinancialAccountNode);
                }
                $paymentMeansNode->appendChild($paymentMandateNode);
            }

            if($paymentMeansNode->childNodes->count() > 0) {
                $nodes[] = $paymentMeansNode;
            }
        }
        return $nodes;
    }

    private function getAllowanceChangeNodes(Document $document, DOMDocument $dom): array
    {
        $allowanceAndCharges = $document->getAllowanceAndCharges();
        $currencyCode = $document->getCurrencyCode() ?? null;

        $nodes = array();
        foreach ($allowanceAndCharges as $allowanceAndCharge) {
            $allowanceChargeNode = $dom->createElement('cac:AllowanceCharge');
            if(!empty($allowanceAndCharge->getChargeIndicator())) {
                $chargeIndicatorNode = $dom->createElement('cbc:ChargeIndicator', $this->xmlEscape($allowanceAndCharge->getChargeIndicator()));
                $allowanceChargeNode->appendChild($chargeIndicatorNode);
            }
            if(!empty($allowanceAndCharge->getAllowanceChargeReason())) {
                $reasonNode = $dom->createElement('cbc:AllowanceChargeReason', $this->xmlEscape($allowanceAndCharge->getAllowanceChargeReason()));
                $allowanceChargeNode->appendChild($reasonNode);
            }
            if(!is_null($allowanceAndCharge->getAmount())) {
                $amountNode = $dom->createElement('cbc:Amount', $this->xmlEscape($allowanceAndCharge->getAmount()));
                if(!empty($currencyCode)) {
                    $amountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
                }
                $allowanceChargeNode->appendChild($amountNode);
            }
            if(!empty($allowanceAndCharge->getTaxCategory())) {
                $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($allowanceAndCharge->getTaxCategory()));
                $taxCategoryNode = $dom->createElement('cac:TaxCategory');
                $taxCategoryNode->appendChild($idNode);
                $allowanceChargeNode->appendChild($taxCategoryNode);

                if($allowanceAndCharge->getTaxCategoryPercent() !== null) {
                    $percentNode = $dom->createElement('cbc:Percent', $this->xmlEscape($allowanceAndCharge->getTaxCategoryPercent()));
                    $taxCategoryNode->appendChild($percentNode);
                }

                if(!empty($allowanceAndCharge->getTaxScheme())) {
                    $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($allowanceAndCharge->getTaxScheme()));
                    $taxSchemeNode = $dom->createElement('cac:TaxScheme');
                    $taxSchemeNode->appendChild($idNode);
                    $taxCategoryNode->appendChild($taxSchemeNode);
                }
            }

            if($allowanceChargeNode->childNodes->count() > 0) {
                $nodes[] = $allowanceChargeNode;
            }
        }
        return $nodes;
    }

    private function getTaxTotalNode(Document $document, DOMDocument $dom): ?DOMNode
    {
        $currencyCode = $document->getCurrencyCode() ?? null;

        $taxTotalNode = $dom->createElement('cac:TaxTotal');

        // TaxAmount
        if(!is_null($document->getTaxTotalAmount())) {
            $taxAmountNode = $dom->createElement('cbc:TaxAmount', $this->xmlEscape($document->getTaxTotalAmount()));
            if(!empty($currencyCode)) {
                $taxAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
            }
            $taxTotalNode->appendChild($taxAmountNode);
        }

        // TaxSubtotal
        $taxSubtotals = $document->getTaxSubtotals();

        foreach ($taxSubtotals as $taxSubtotal) {
            $taxSubtotalNode = $dom->createElement('cac:TaxSubtotal');
            if(!is_null($taxSubtotal->getTaxableAmount())) {
                $taxableAmountNode = $dom->createElement('cbc:TaxableAmount', $this->xmlEscape($taxSubtotal->getTaxableAmount()));
                if(!empty($currencyCode)) {
                    $taxableAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
                }
                $taxSubtotalNode->appendChild($taxableAmountNode);
            }
            if(!is_null($taxSubtotal->getTaxAmount())) {
                $taxAmountNode = $dom->createElement('cbc:TaxAmount', $this->xmlEscape($taxSubtotal->getTaxAmount()));
                if(!empty($currencyCode)) {
                    $taxAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
                }
                $taxSubtotalNode->appendChild($taxAmountNode);
            }
            $taxCategoryNode = $dom->createElement('cac:TaxCategory');
            if(!is_null($taxSubtotal->getTaxCategoryId())) {
                $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($taxSubtotal->getTaxCategoryId()));
                $taxCategoryNode->appendChild($idNode);
            }
            if(!is_null($taxSubtotal->getTaxCategoryPercent())) {
                $percentNode = $dom->createElement('cbc:Percent', $this->xmlEscape($taxSubtotal->getTaxCategoryPercent()));
                $taxCategoryNode->appendChild($percentNode);
            }
            if(!is_null($taxSubtotal->getTaxCategoryTaxExcemptionReason())) {
                $taxExemptionReasonNode = $dom->createElement('cbc:TaxExemptionReason', $this->xmlEscape($taxSubtotal->getTaxCategoryTaxExcemptionReason()));
                $taxCategoryNode->appendChild($taxExemptionReasonNode);
            }
            $taxSchemeNode = $dom->createElement('cac:TaxScheme');
            $idNode = $dom->createElement('cbc:ID', 'VAT');
            $taxSchemeNode->appendChild($idNode);
            $taxCategoryNode->appendChild($taxSchemeNode);
            if($taxCategoryNode->childNodes->count() > 0) {
                $taxSubtotalNode->appendChild($taxCategoryNode);
            }
            if($taxSubtotalNode->childNodes->count() > 0) {
                $taxTotalNode->appendChild($taxSubtotalNode);
            }
        }

        return $taxTotalNode;
    }

    private function getLegalMonetaryTotalNode(Document $document, DOMDocument $dom): ?DOMNode
    {
        $currencyCode = $document->getCurrencyCode() ?? null;
        $totals = $document->getTotals();
        $legalMonetaryTotalNode = $dom->createElement('cac:LegalMonetaryTotal');

        if(!is_null($totals->getLineExtensionAmount())) {
            $lineExtensionAmountNode = $dom->createElement('cbc:LineExtensionAmount', $this->xmlEscape($totals->getLineExtensionAmount()));
            if(!empty($currencyCode)) {
                $lineExtensionAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
            }
            $legalMonetaryTotalNode->appendChild($lineExtensionAmountNode);
        }
        if(!is_null($totals->getTaxExclusiveAmount())) {
            $taxExclusiveAmountNode = $dom->createElement('cbc:TaxExclusiveAmount', $this->xmlEscape($totals->getTaxExclusiveAmount()));
            if(!empty($currencyCode)) {
                $taxExclusiveAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
            }
            $legalMonetaryTotalNode->appendChild($taxExclusiveAmountNode);
        }
        if(!is_null($totals->getTaxInclusiveAmount())) {
            $taxInclusiveAmountNode = $dom->createElement('cbc:TaxInclusiveAmount', $this->xmlEscape($totals->getTaxInclusiveAmount()));
            if(!empty($currencyCode)) {
                $taxInclusiveAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
            }
            $legalMonetaryTotalNode->appendChild($taxInclusiveAmountNode);
        }
        if(!is_null($totals->getAllowanceTotalAmount())) {
            $allowanceTotalAmountNode = $dom->createElement('cbc:AllowanceTotalAmount', $this->xmlEscape($totals->getAllowanceTotalAmount()));
            if(!empty($currencyCode)) {
                $allowanceTotalAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
            }
            $legalMonetaryTotalNode->appendChild($allowanceTotalAmountNode);
        }
        if(!is_null($totals->getChargeTotalAmount())) {
            $chargeTotalAmountNode = $dom->createElement('cbc:ChargeTotalAmount', $this->xmlEscape($totals->getChargeTotalAmount()));
            if(!empty($currencyCode)) {
                $chargeTotalAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
            }
            $legalMonetaryTotalNode->appendChild($chargeTotalAmountNode);
        }
        if(!is_null($totals->getPrepaidAmount())) {
            $prepaidAmountNode = $dom->createElement('cbc:PrepaidAmount', $this->xmlEscape($totals->getPrepaidAmount()));
            if(!empty($currencyCode)) {
                $prepaidAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
            }
            $legalMonetaryTotalNode->appendChild($prepaidAmountNode);
        }
        if(!is_null($totals->getPayableRoundingAmount())) {
            $payableRoundingAmountNode = $dom->createElement('cbc:PayableRoundingAmount', $this->xmlEscape($totals->getPayableRoundingAmount()));
            if(!empty($currencyCode)) {
                $payableRoundingAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
            }
            $legalMonetaryTotalNode->appendChild($payableRoundingAmountNode);
        }
        if(!is_null($totals->getPayableAmount())) {
            $payableAmountNode = $dom->createElement('cbc:PayableAmount', $this->xmlEscape($totals->getPayableAmount()));
            if(!empty($currencyCode)) {
                $payableAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
            }
            $legalMonetaryTotalNode->appendChild($payableAmountNode);
        }

        return $legalMonetaryTotalNode->childNodes->count() > 0 ? $legalMonetaryTotalNode : null;
    }

    private function getInvoiceLineAndCreditNoteLineNodes(Document $document, DOMDocument $dom): array
    {
        $currencyCode = $document->getCurrencyCode() ?? null;
        $lines = $document->getLines();
        $nodes = array();

        foreach ($lines as $line) {
            $documentLineNode = null;
            if($document->getDocType() === "invoice") {
                $documentLineNode = $dom->createElement('cac:InvoiceLine');
            } else if(in_array($document->getDocType(), ["creditnote", "credit_note"])) {
                $documentLineNode = $dom->createElement('cac:CreditNoteLine');
            }
            if($documentLineNode) {
                if(!is_null($line->getId())) {
                    $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($line->getId()));
                    $documentLineNode->appendChild($idNode);
                }
                if(!empty($line->getNote())) {
                    $noteNode = $dom->createElement('cbc:Note', $this->xmlEscape($line->getNote()));
                    $documentLineNode->appendChild($noteNode);
                }
                if(!is_null($line->getQuantity())) {
                    $quantityNode = null;
                    if($document->getDocType() === "invoice") {
                        $quantityNode = $dom->createElement('cbc:InvoicedQuantity', $this->xmlEscape($line->getQuantity()));
                    } else if(in_array($document->getDocType(), ["creditnote", "credit_note"])) {
                        $quantityNode = $dom->createElement('cbc:CreditedQuantity', $this->xmlEscape($line->getQuantity()));
                    }
                    if($quantityNode) {
                        if(!empty($line->getUnitCode())) {
                            $quantityNode->setAttribute('unitCode', $this->xmlEscape($line->getUnitCode()));
                        }
                        $documentLineNode->appendChild($quantityNode);
                    }
                }
                if(!is_null($line->getLineExtensionAmount())) {
                    $lineExtensionAmountNode = $dom->createElement('cbc:LineExtensionAmount', $this->xmlEscape($line->getLineExtensionAmount()));
                    if(!empty($currencyCode)) {
                        $lineExtensionAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
                    }
                    $documentLineNode->appendChild($lineExtensionAmountNode);
                }
                if(!empty($line->getAccountingCost())) {
                    $accountingCostNode = $dom->createElement('cbc:AccountingCost', $this->xmlEscape($line->getAccountingCost()));
                    $documentLineNode->appendChild($accountingCostNode);
                }
                if(!empty($line->getInvoicePeriodStartDate()) && !empty($line->getInvoicePeriodEndDate())) {
                    $startDateNode = $dom->createElement('cbc:StartDate', $this->xmlEscape($line->getInvoicePeriodStartDate()));
                    $endDateNode = $dom->createElement('cbc:EndDate', $this->xmlEscape($line->getInvoicePeriodEndDate()));
                    $invoicePeriodNode = $dom->createElement('cac:InvoicePeriod');
                    $invoicePeriodNode->appendChild($startDateNode);
                    $invoicePeriodNode->appendChild($endDateNode);
                    $documentLineNode->appendChild($invoicePeriodNode);
                }
                if(!empty($line->getOrderLineReferenceLineId())) {
                    $lineIdNode = $dom->createElement('cbc:LineID', $this->xmlEscape($line->getOrderLineReferenceLineId()));
                    $orderLineReferenceNode = $dom->createElement('cac:OrderLineReference');
                    $orderLineReferenceNode->appendChild($lineIdNode);
                    $documentLineNode->appendChild($orderLineReferenceNode);
                }

                foreach ($line->getAllowanceAndCharges() as $allowanceAndCharge) {
                    $allowanceChargeNode = $dom->createElement('cac:AllowanceCharge');
                    if(!empty($allowanceAndCharge->getAcIndicator())) {
                        $chargeIndicatorNode = $dom->createElement('cbc:ChargeIndicator', $this->xmlEscape($allowanceAndCharge->getAcIndicator()));
                        $allowanceChargeNode->appendChild($chargeIndicatorNode);
                    }
                    if(!empty($allowanceAndCharge->getAcReasonCode())) {
                        $allowanceChargeReasonCodeNode = $dom->createElement('cbc:AllowanceChargeReasonCode', $this->xmlEscape($allowanceAndCharge->getAcReasonCode()));
                        $allowanceChargeNode->appendChild($allowanceChargeReasonCodeNode);
                    }
                    if(!empty($allowanceAndCharge->getAcAllowanceChargeReason())) {
                        $reasonNode = $dom->createElement('cbc:AllowanceChargeReason', $this->xmlEscape($allowanceAndCharge->getAcAllowanceChargeReason()));
                        $allowanceChargeNode->appendChild($reasonNode);
                    }
                    if(!empty($allowanceAndCharge->getAcMultiplierFactorNumeric())) {
                        $multiplierFactorNumericNode = $dom->createElement('cbc:MultiplierFactorNumeric', $this->xmlEscape($allowanceAndCharge->getAcMultiplierFactorNumeric()));
                        $allowanceChargeNode->appendChild($multiplierFactorNumericNode);
                    }
                    if(!is_null($allowanceAndCharge->getAcAmount())) {
                        $amountNode = $dom->createElement('cbc:Amount', $this->xmlEscape($allowanceAndCharge->getAcAmount()));
                        if(!empty($currencyCode)) {
                            $amountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
                        }
                        $allowanceChargeNode->appendChild($amountNode);
                    }
                    if(!is_null($allowanceAndCharge->getAcBaseAmount())) {
                        $baseAmountNode = $dom->createElement('cbc:BaseAmount', $this->xmlEscape($allowanceAndCharge->getAcBaseAmount()));
                        if(!empty($currencyCode)) {
                            $baseAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
                        }
                        $allowanceChargeNode->appendChild($baseAmountNode);
                    }
                    if($allowanceChargeNode->childNodes->count() > 0) {
                        $documentLineNode->appendChild($allowanceChargeNode);
                    }
                }

                $itemNode = $dom->createElement('cac:Item');
                if(!empty($line->getDescription())) {
                    $descriptionNode = $dom->createElement('cbc:Description', $this->xmlEscape($line->getDescription()));
                    $itemNode->appendChild($descriptionNode);
                }
                if(!empty($line->getName())) {
                    $nameNode = $dom->createElement('cbc:Name', $this->xmlEscape($line->getName()));
                    $itemNode->appendChild($nameNode);
                }
                if(!empty($line->getBuyersItemIdentificationId())) {
                    $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($line->getBuyersItemIdentificationId()));
                    $buyersItemIdentificationNode = $dom->createElement('cac:BuyersItemIdentification');
                    $buyersItemIdentificationNode->appendChild($idNode);
                    $itemNode->appendChild($buyersItemIdentificationNode);
                }
                if(!empty($line->getSellersItemIdentificationId())) {
                    $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($line->getSellersItemIdentificationId()));
                    $sellersItemIdentificationNode = $dom->createElement('cac:SellersItemIdentification');
                    $sellersItemIdentificationNode->appendChild($idNode);
                    $itemNode->appendChild($sellersItemIdentificationNode);
                }
                if(!empty($line->getStandardItemIdentificationId())) {
                    $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($line->getStandardItemIdentificationId()));
                    $standardItemIdentificationNode = $dom->createElement('cac:StandardItemIdentification');
                    $standardItemIdentificationNode->appendChild($idNode);
                    $itemNode->appendChild($standardItemIdentificationNode);
                }
                $classifiedTaxCategoryNode = $dom->createElement('cac:ClassifiedTaxCategory');
                if(!empty($line->getTaxCategoryId())) {
                    $idNode = $dom->createElement('cbc:ID', $this->xmlEscape($line->getTaxCategoryId()));
                    $classifiedTaxCategoryNode->appendChild($idNode);
                }
                if(!is_null($line->getTaxCategoryPercent())) {
                    $percentNode = $dom->createElement('cbc:Percent', $this->xmlEscape($line->getTaxCategoryPercent()));
                    $classifiedTaxCategoryNode->appendChild($percentNode);
                }
                $taxSchemeNode = $dom->createElement('cac:TaxScheme');
                $idNode = $dom->createElement('cbc:ID', 'VAT');
                $taxSchemeNode->appendChild($idNode);
                $classifiedTaxCategoryNode->appendChild($taxSchemeNode);
                if($classifiedTaxCategoryNode->childNodes->count() > 0) {
                    $itemNode->appendChild($classifiedTaxCategoryNode);
                }
                if($itemNode->childNodes->count() > 0) {
                    $documentLineNode->appendChild($itemNode);
                }
                $priceNode = $dom->createElement('cac:Price');
                if(!is_null($line->getPrice())) {
                    $priceAmountNode = $dom->createElement('cbc:PriceAmount', $this->xmlEscape($line->getPrice()));
                    if(!empty($currencyCode)) {
                        $priceAmountNode->setAttribute('currencyID', $this->xmlEscape($currencyCode));
                    }
                    $priceNode->appendChild($priceAmountNode);
                }
                if(!is_null($line->getBaseQuantity())) {
                    $baseQuantityNode = $dom->createElement('cbc:BaseQuantity', $this->xmlEscape($line->getBaseQuantity()));
                    if(!empty($line->getBaseQuantityUnitCode())) {
                        $baseQuantityNode->setAttribute('unitCode', $this->xmlEscape($line->getBaseQuantityUnitCode()));
                    }
                    $priceNode->appendChild($baseQuantityNode);
                }
                if(!is_null($line->getPriceAllowanceChargeIndicator()) &&
                    !is_null($line->getPriceAllowanceChargeAmount())) {
                    $priceAllowanceCharge = $dom->createElement('cac:AllowanceCharge');

                    $priceChargeIndicatorNode = $dom->createElement('cbc:ChargeIndicator', $this->xmlEscape($line->getPriceAllowanceChargeIndicator()));
                    $priceAllowanceCharge->appendChild($priceChargeIndicatorNode);

                    $priceAllowanceChargeAmount = $dom->createElement('cbc:Amount', $this->xmlEscape($line->getPriceAllowanceChargeAmount()));
                    if(!empty($currencyCode)) {
                        $priceAllowanceChargeAmount->setAttribute('currencyID', $this->xmlEscape($currencyCode));
                    }
                    $priceAllowanceCharge->appendChild($priceAllowanceChargeAmount);

                    if (!is_null($line->getPriceAllowanceChargeBaseAmount()))
                    {
                        $priceAllowanceChargeBaseAmount = $dom->createElement('cbc:BaseAmount', $this->xmlEscape($line->getPriceAllowanceChargeBaseAmount()));
                        if(!empty($currencyCode)) {
                            $priceAllowanceChargeBaseAmount->setAttribute('currencyID', $this->xmlEscape($currencyCode));
                        }
                        $priceAllowanceCharge->appendChild($priceAllowanceChargeBaseAmount);
                    }

                    $priceNode->appendChild($priceAllowanceCharge);
                }
                if($priceNode->childNodes->count() > 0) {
                    $documentLineNode->appendChild($priceNode);
                }

                if($documentLineNode->childNodes->count() > 0) {
                    $nodes[] = $documentLineNode;
                }
            }
        }

        return $nodes;
    }
}
