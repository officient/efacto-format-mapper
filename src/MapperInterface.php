<?php

namespace Officient\EfactoMapper;

interface MapperInterface
{
    const versions = ['2.2', '2.3', '2.4'];

    public function mapToXRechnung(string $data): \DOMDocument;
    public function mapToXRechnungString(string $data): string;
    public function mapToPeppol3(string $data): \DOMDocument;
    public function mapToPeppol3String(string $data): string;
}