<?php

namespace Officient\EfactoMapper\Model;

class PaymentMandate
{
    /** @var string|null */
    protected ?string $id;
    /** @var PayerFinancialAccount|null */
    protected ?PayerFinancialAccount $payerFinancialAccount;

    /**
     * @param string|null $id
     * @param PayerFinancialAccount|null $payerFinancialAccount
     */
    public function __construct(?string $id, ?PayerFinancialAccount $payerFinancialAccount)
    {
        $this->id = $id;
        $this->payerFinancialAccount = $payerFinancialAccount;
    }

    /**
     * @return string|null
     */
    public function getID(): ?string
    {
        return $this->id;
    }

    /**
     * @return PayerFinancialAccount|null
     */
    public function getPayerFinancialAccount(): ?PayerFinancialAccount
    {
        return $this->payerFinancialAccount;
    }
}