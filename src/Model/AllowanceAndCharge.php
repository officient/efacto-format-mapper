<?php

namespace Officient\EfactoMapper\Model;

class AllowanceAndCharge
{
    /** @var string|null */
    protected ?string $chargeIndicator;
    /** @var string|null */
    protected ?string $amount;
    /** @var string|null */
    protected ?string $taxCategory;
    /** @var string|null */
    protected ?string $taxCategoryPercent;
    /** @var string|null */
    protected ?string $taxScheme;
    /** @var string|null */
    protected ?string $allowanceChargeReason;

    /**
     * @param string|null $chargeIndicator
     * @param string|null $amount
     * @param string|null $taxCategory
     * @param string|null $taxCategoryPercent
     * @param string|null $taxScheme
     * @param string|null $allowanceChargeReason
     */
    public function __construct(?string $chargeIndicator, ?string $amount, ?string $taxCategory, ?string $taxCategoryPercent, ?string $taxScheme, ?string $allowanceChargeReason)
    {
        $this->chargeIndicator = $chargeIndicator;
        $this->amount = $amount;
        $this->taxCategory = $taxCategory;
        $this->taxCategoryPercent = $taxCategoryPercent;
        $this->taxScheme = $taxScheme;
        $this->allowanceChargeReason = $allowanceChargeReason;
    }

    /**
     * @return string|null
     */
    public function getChargeIndicator(): ?string
    {
        return $this->chargeIndicator;
    }

    /**
     * @return string|null
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * @return string|null
     */
    public function getTaxCategory(): ?string
    {
        return $this->taxCategory;
    }

    /**
     * @return string|null
     */
    public function getTaxCategoryPercent(): ?string
    {
        return $this->taxCategoryPercent;
    }

    /**
     * @return string|null
     */
    public function getTaxScheme(): ?string
    {
        return $this->taxScheme;
    }

    /**
     * @return string|null
     */
    public function getAllowanceChargeReason(): ?string
    {
        return $this->allowanceChargeReason;
    }
}