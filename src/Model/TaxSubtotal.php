<?php

namespace Officient\EfactoMapper\Model;

class TaxSubtotal
{
    /** @var string|null */
    protected ?string $taxableAmount;
    /** @var string|null */
    protected ?string $taxAmount;
    /** @var string|null */
    protected ?string $taxCategoryId;
    /** @var string|null */
    protected ?string $taxCategoryPercent;
    /** @var string|null */
    protected ?string $taxCategoryTaxExcemptionReason;

    /**
     * @param string|null $taxableAmount
     * @param string|null $taxAmount
     * @param string|null $taxCategoryId
     * @param string|null $taxCategoryPercent
     * @param string|null $taxCategoryTaxExcemptionReason
     */
    public function __construct(?string $taxableAmount, ?string $taxAmount, ?string $taxCategoryId, ?string $taxCategoryPercent, ?string $taxCategoryTaxExcemptionReason)
    {
        $this->taxableAmount = $taxableAmount;
        $this->taxAmount = $taxAmount;
        $this->taxCategoryId = $taxCategoryId;
        $this->taxCategoryPercent = $taxCategoryPercent;
        $this->taxCategoryTaxExcemptionReason = $taxCategoryTaxExcemptionReason;
    }

    /**
     * @return string|null
     */
    public function getTaxableAmount(): ?string
    {
        return $this->taxableAmount;
    }

    /**
     * @return string|null
     */
    public function getTaxAmount(): ?string
    {
        return $this->taxAmount;
    }

    /**
     * @return string|null
     */
    public function getTaxCategoryId(): ?string
    {
        return $this->taxCategoryId;
    }

    /**
     * @return string|null
     */
    public function getTaxCategoryPercent(): ?string
    {
        return $this->taxCategoryPercent;
    }

    /**
     * @return string|null
     */
    public function getTaxCategoryTaxExcemptionReason(): ?string
    {
        return $this->taxCategoryTaxExcemptionReason;
    }
}