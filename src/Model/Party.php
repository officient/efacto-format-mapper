<?php

namespace Officient\EfactoMapper\Model;

class Party
{
    /** @var string|null */
    protected ?string $endpointSchemeId;
    /** @var string|null */
    protected ?string $endpointId;
    /** @var string|null */
    protected ?string $partyIdentificationSchemeId;
    /** @var string|null */
    protected ?string $partyIdentificationId;
    /** @var string|null */
    protected ?string $name;
    /** @var string|null */
    protected ?string $streetName;
    /** @var string|null */
    protected ?string $cityName;
    /** @var string|null */
    protected ?string $postalZone;
    /** @var string|null */
    protected ?string $countryCode;
    /** @var string|null */
    protected ?string $partyTaxSchemeCompanyId;
    /** @var string|null */
    protected ?string $partyLegalEntityCompanySchemeId;
    /** @var string|null */
    protected ?string $partyLegalEntityCompanyId;
    /** @var string|null */
    protected ?string $contactName;
    /** @var string|null */
    protected ?string $contactTelephone;
    /** @var string|null */
    protected ?string $contactElectronicMail;

    /**
     * @param string|null $endpointSchemeId
     * @param string|null $endpointId
     * @param string|null $partyIdentificationSchemeId
     * @param string|null $partyIdentificationId
     * @param string|null $name
     * @param string|null $streetName
     * @param string|null $cityName
     * @param string|null $postalZone
     * @param string|null $countryCode
     * @param string|null $partyTaxSchemeCompanyId
     * @param string|null $partyLegalEntityCompanySchemeId
     * @param string|null $partyLegalEntityCompanyId
     * @param string|null $contactName
     * @param string|null $contactTelephone
     * @param string|null $contactElectronicMail
     */
    public function __construct(?string $endpointSchemeId, ?string $endpointId, ?string $partyIdentificationSchemeId, ?string $partyIdentificationId, ?string $name, ?string $streetName, ?string $cityName, ?string $postalZone, ?string $countryCode, ?string $partyTaxSchemeCompanyId, ?string $partyLegalEntityCompanySchemeId, ?string $partyLegalEntityCompanyId, ?string $contactName, ?string $contactTelephone, ?string $contactElectronicMail)
    {
        $this->endpointSchemeId = $endpointSchemeId;
        $this->endpointId = $endpointId;
        $this->partyIdentificationSchemeId = $partyIdentificationSchemeId;
        $this->partyIdentificationId = $partyIdentificationId;
        $this->name = $name;
        $this->streetName = $streetName;
        $this->cityName = $cityName;
        $this->postalZone = $postalZone;
        $this->countryCode = $countryCode;
        $this->partyTaxSchemeCompanyId = $partyTaxSchemeCompanyId;
        $this->partyLegalEntityCompanySchemeId = $partyLegalEntityCompanySchemeId;
        $this->partyLegalEntityCompanyId = $partyLegalEntityCompanyId;
        $this->contactName = $contactName;
        $this->contactTelephone = $contactTelephone;
        $this->contactElectronicMail = $contactElectronicMail;
    }

    /**
     * @return string|null
     */
    public function getEndpointSchemeId(): ?string
    {
        return $this->endpointSchemeId;
    }

    /**
     * @return string|null
     */
    public function getEndpointId(): ?string
    {
        return $this->endpointId;
    }

    /**
     * @return string|null
     */
    public function getPartyIdentificationSchemeId(): ?string
    {
        return $this->partyIdentificationSchemeId;
    }

    /**
     * @return string|null
     */
    public function getPartyIdentificationId(): ?string
    {
        return $this->partyIdentificationId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    /**
     * @return string|null
     */
    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    /**
     * @return string|null
     */
    public function getPostalZone(): ?string
    {
        return $this->postalZone;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @return string|null
     */
    public function getPartyTaxSchemeCompanyId(): ?string
    {
        return $this->partyTaxSchemeCompanyId;
    }

    /**
     * @return string|null
     */
    public function getPartyLegalEntityCompanySchemeId(): ?string
    {
        return $this->partyLegalEntityCompanySchemeId;
    }

    /**
     * @return string|null
     */
    public function getPartyLegalEntityCompanyId(): ?string
    {
        return $this->partyLegalEntityCompanyId;
    }

    /**
     * @return string|null
     */
    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    /**
     * @return string|null
     */
    public function getContactTelephone(): ?string
    {
        return $this->contactTelephone;
    }

    /**
     * @return string|null
     */
    public function getContactElectronicMail(): ?string
    {
        return $this->contactElectronicMail;
    }
}