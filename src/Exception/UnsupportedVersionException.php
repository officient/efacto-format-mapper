<?php

namespace Officient\EfactoMapper\Exception;

use Throwable;

class UnsupportedVersionException extends \Exception
{
    public function __construct($message = "The version is not supported", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}