<?php

namespace Officient\EfactoMapper\Model;

class Document
{
    /** @var string|null */
    protected ?string $docNumber;
    /** @var string|null */
    protected ?string $docType;
    /** @var string|null */
    protected ?string $docFormat;
    /** @var string|null */
    protected ?string $issueDate;
    /** @var string|null */
    protected ?string $dueDate;
    /** @var string|null */
    protected ?string $invoiceTypeCode;
    /** @var string|null */
    protected ?string $note;
    /** @var string|null */
    protected ?string $currencyCode;
    /** @var References */
    protected References $references;
    /** @var Supplier */
    protected Supplier $supplier;
    /** @var Customer */
    protected Customer $customer;
    /** @var Delivery */
    protected Delivery $delivery;
    /** @var Payment[] */
    protected array $payments;
    /** @var string|null */
    protected ?string $paymentTermsNote;
    /** @var AllowanceAndCharge[] */
    protected array $allowanceAndCharges;
    /** @var string|null */
    protected ?string $taxTotalAmount;
    /** @var TaxSubtotal[] */
    protected array $taxSubtotals;
    /** @var Totals */
    protected Totals $totals;
    /** @var Line[] */
    protected array $lines;
    /** @var Attachment[] */
    protected array $attachments;

    /**
     * @param string|null $docNumber
     * @param string|null $docType
     * @param string|null $docFormat
     * @param string|null $issueDate
     * @param string|null $dueDate
     * @param string|null $invoiceTypeCode
     * @param string|null $note
     * @param string|null $currencyCode
     * @param References $references
     * @param Supplier $supplier
     * @param Customer $customer
     * @param Delivery $delivery
     * @param Payment[] $payments
     * @param string|null $paymentTermsNote
     * @param AllowanceAndCharge[] $allowanceAndCharges
     * @param string|null $taxTotalAmount
     * @param TaxSubtotal[] $taxSubtotals
     * @param Totals $totals
     * @param Line[] $lines
     * @param Attachment[] $attachments
     */
    public function __construct(?string $docNumber, ?string $docType, ?string $docFormat, ?string $issueDate, ?string $dueDate, ?string $invoiceTypeCode, ?string $note, ?string $currencyCode, References $references, Supplier $supplier, Customer $customer, Delivery $delivery, array $payments, ?string $paymentTermsNote, array $allowanceAndCharges, ?string $taxTotalAmount, array $taxSubtotals, Totals $totals, array $lines, array $attachments)
    {
        $this->docNumber = $docNumber;
        $this->docType = strtolower($docType);
        $this->docFormat = $docFormat;
        $this->issueDate = $issueDate;
        $this->dueDate = $dueDate;
        $this->invoiceTypeCode = $invoiceTypeCode;
        $this->note = $note;
        $this->currencyCode = $currencyCode;
        $this->references = $references;
        $this->supplier = $supplier;
        $this->customer = $customer;
        $this->delivery = $delivery;
        $this->payments = $payments;
        $this->paymentTermsNote = $paymentTermsNote;
        $this->allowanceAndCharges = $allowanceAndCharges;
        $this->taxTotalAmount = $taxTotalAmount;
        $this->taxSubtotals = $taxSubtotals;
        $this->totals = $totals;
        $this->lines = $lines;
        $this->attachments = $attachments;
    }

    /**
     * @return string|null
     */
    public function getDocNumber(): ?string
    {
        return $this->docNumber;
    }

    /**
     * @return string|null
     */
    public function getDocType(): ?string
    {
        return $this->docType;
    }

    /**
     * @return string|null
     */
    public function getIssueDate(): ?string
    {
        return $this->issueDate;
    }

    /**
     * @return string|null
     */
    public function getDueDate(): ?string
    {
        return $this->dueDate;
    }

    /**
     * @return string|null
     */
    public function getInvoiceTypeCode(): ?string
    {
        return $this->invoiceTypeCode;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @return string|null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @return References
     */
    public function getReferences(): References
    {
        return $this->references;
    }

    /**
     * @return Supplier
     */
    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return Delivery
     */
    public function getDelivery(): Delivery
    {
        return $this->delivery;
    }

    /**
     * @return Payment[]
     */
    public function getPayments(): array
    {
        return $this->payments;
    }

    /**
     * @return string|null
     */
    public function getPaymentTermsNote(): ?string
    {
        return $this->paymentTermsNote;
    }

    /**
     * @return AllowanceAndCharge[]
     */
    public function getAllowanceAndCharges(): array
    {
        return $this->allowanceAndCharges;
    }

    /**
     * @return string|null
     */
    public function getTaxTotalAmount(): ?string
    {
        return $this->taxTotalAmount;
    }

    /**
     * @return TaxSubtotal[]
     */
    public function getTaxSubtotals(): array
    {
        return $this->taxSubtotals;
    }

    /**
     * @return Totals
     */
    public function getTotals(): Totals
    {
        return $this->totals;
    }

    /**
     * @return Line[]
     */
    public function getLines(): array
    {
        return $this->lines;
    }

    /**
     * @return Attachment[]
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }
}