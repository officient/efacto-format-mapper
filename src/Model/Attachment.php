<?php

namespace Officient\EfactoMapper\Model;

class Attachment
{
    /** @var string|null */
    protected ?string $mimeCode;
    /** @var string|null */
    protected ?string $id;
    /** @var string|null */
    protected ?string $filename;
    /** @var string|null */
    protected ?string $documentType;
    /** @var string|null */
    protected ?string $document;

    /**
     * @param string|null $mimeCode
     * @param string|null $id
     * @param string|null $filename
     * @param string|null $documentType
     * @param string|null $document
     */
    public function __construct(?string $mimeCode, ?string $id, ?string $filename, ?string $documentType, ?string $document)
    {
        $this->mimeCode = $mimeCode;
        $this->id = $id;
        $this->filename = $filename;
        $this->documentType = $documentType;
        $this->document = $document;
    }

    /**
     * @return string|null
     */
    public function getMimeCode(): ?string
    {
        return $this->mimeCode;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @return string|null
     */
    public function getDocumentType(): ?string
    {
        return $this->documentType;
    }

    /**
     * @return string|null
     */
    public function getDocument(): ?string
    {
        return $this->document;
    }
}