<?php

namespace Officient\EfactoMapper;

use DOMDocument;
use Officient\EfactoMapper\Exception\UnsupportedFormatException;
use Officient\EfactoMapper\Exception\UnsupportedVersionException;
use Officient\EfactoMapper\Model\Attachment;
use Officient\EfactoMapper\Model\AllowanceAndCharge;
use Officient\EfactoMapper\Model\Customer;
use Officient\EfactoMapper\Model\Delivery;
use Officient\EfactoMapper\Model\Document;
use Officient\EfactoMapper\Model\Line;
use Officient\EfactoMapper\Model\LineAllowanceAndCharge;
use Officient\EfactoMapper\Model\Payment;
use Officient\EfactoMapper\Model\References;
use Officient\EfactoMapper\Model\Supplier;
use Officient\EfactoMapper\Model\TaxSubtotal;
use Officient\EfactoMapper\Model\Totals;
use Officient\EfactoMapper\Model\PaymentMandate;
use Officient\EfactoMapper\Model\PayerFinancialAccount;

class JSONMapper implements MapperInterface
{
    public function mapToXRechnung(string $data): DOMDocument
    {
        // Verify support and get meta data
        $metadata = $this->findMetadata($data);
        if(!$this->supports($metadata)) {
            throw new UnsupportedFormatException();
        }

        // Build document object
        $document = $this->getDocument($data, $metadata);

        return (new XRechnungExporter())->export($document);
    }

    public function mapToXRechnungString(string $data): string
    {
        return $this->mapToXRechnung($data)->saveXML();
    }

    public function mapToPeppol3(string $data): DOMDocument
    {
        // Verify support and get meta data
        $metadata = $this->findMetadata($data);
        if(!$this->supports($metadata)) {
            throw new UnsupportedFormatException();
        }

        // Build document object
        $document = $this->getDocument($data, $metadata);

        return (new Peppol3Exporter())->export($document);
    }

    public function mapToPeppol3String(string $data): string
    {
        return $this->mapToPeppol3($data)->saveXML();
    }

    private function getDocument(string $data, array $metadata)
    {
        switch ($metadata['Version']) {
            case '2.2':
                return $this->getDocument22($data);
            case '2.3':
                return $this->getDocument23($data);
            case '2.4':
                return $this->getDocument24($data);
            default:
                throw new UnsupportedVersionException();
        }
    }

    private function getDocument22(string $json): Document
    {
        $jsonArray = $this->jsonToArray($json);
        $docHeader = $this->findOneInJson('DocumentHeader', $jsonArray);
        $docLines = $this->findOneInJson('DocumentLines', $jsonArray);
        $docAttachments = $this->findOneInJson('DocumentAttachments', $jsonArray);

        // Assemble attachments
        $attachments = array();
        foreach ($docAttachments as $docAttachment) {
            $attachments[] = new Attachment(
                $docAttachment['MimeCode'] ?? null,
                $docAttachment['Id'] ?? null,
                $docAttachment['Filename'] ?? null,
                $docAttachment['DocumentType'] ?? null,
                $docAttachment['Document'] ?? null
            );
        }

        // Assemble allowance and charges
        $allowanceAndCharges = array();
        foreach (($docHeader['AllowanceAndCharges'] ?? []) as $ac) {
            $allowanceAndCharges[] = new AllowanceAndCharge(
                $ac['ChargeIndicator'] ?? null,
                $ac['Amount'] ?? null,
                $ac['TaxCategory'] ?? null,
                null,
                $ac['TaxScheme'] ?? null,
                null
            );
        }

        // Assemble payments
        $payments = array();
        foreach (($docHeader['Payments'] ?? []) as $payment) {
            $payments[] = new Payment(
                '31',
                $payment['PaymentID'] ?? null,
                $payment['PayeeFinancialAccountID'] ?? null,
                $payment['PayeeFinancialAccountName'] ?? null,
                $payment['FinancialInstitutionBranchID'] ?? null,
                null
            );
        }

        // Assemble taxSubtotals
        $taxSubtotals = array();
        foreach (($docHeader['Tax']['SubTotals'] ?? []) as $taxSubTotal) {
            $taxSubtotals[] = new TaxSubtotal(
                $taxSubTotal['TaxableAmount'] ?? null,
                $taxSubTotal['TaxAmount'] ?? null,
                $taxSubTotal['TaxCategoryID'] ?? null,
                $taxSubTotal['TaxCategoryPercent'] ?? null,
                $taxSubTotal['TaxCategoryTaxExemptionReason'] ?? null
            );
        }

        // Assemble lines
        $lines = array();
        foreach ($docLines as $docLine) {
            // Assemble line allowance and charges
            $lineAllowanceAndCharges = array();
            foreach (($docLine['AllowanceAndCharges'] ?? []) as $ac) {
                $lineAllowanceAndCharges[] = new LineAllowanceAndCharge(
                    $ac['ChargeIndicator'] ?? null,
                    $ac['Amount'] ?? null,
                    $ac['ReasonCode'] ?? null,
                    null,
                    null,
                    null
                );
            }

            $lines[] = new Line(
                $docLine['ID'] ?? null,
                $docLine['Note'] ?? null,
                $docLine['Quantity'] ?? null,
                $docLine['UnitCode'] ?? null,
                $docLine['LineExtensionAmount'] ?? null,
                $docLine['AccountingCost'] ?? null,
                $docLine['InvoicePeriodStartDate'] ?? null,
                $docLine['InvoicePeriodEndDate'] ?? null,
                $docLine['OrderLineReferenceLineID'] ?? null,
                $docLine['BuyersItemIdentificationID'] ?? null,
                $docLine['SellersItemIdentificationID'] ?? null,
                $docLine['StandardItemIdentificationID'] ?? null,
                $docLine['Description'] ?? null,
                $docLine['Name'] ?? null,
                $docLine['TaxCategoryID'] ?? null,
                $docLine['TaxCategoryPercent'] ?? null,
                $lineAllowanceAndCharges,
                $docLine['Price'] ?? null,
                $docLine['BaseQuantity'] ?? null,
                $docLine['BaseQuantityUnitCode'] ?? null,
                null,
                null,
                null
            );
        }

        // Assemble document
        return new Document(
            $docHeader['Document']['DocNumber'] ?? null,
            $docHeader['Document']['DocType'] ?? null,
            $docHeader['Document']['DocFormat'] ?? null,
            $docHeader['Document']['IssueDate'] ?? null,
            $docHeader['Document']['DueDate'] ?? null,
            null,
            $docHeader['Document']['Note'] ?? null,
            $docHeader['Document']['CurrencyCode'] ?? null,
            new References(
                $docHeader['References']['AccountingCost'] ?? null,
                $docHeader['References']['BuyerReference'] ?? null,
                $docHeader['References']['InvoicePeriodStartDate'] ?? null,
                $docHeader['References']['InvoicePeriodEndDate'] ?? null,
                $docHeader['References']['OrderReference'] ?? null,
                $docHeader['References']['ContractDocumentReference'] ?? null,
                $docHeader['References']['ProjectReference'] ?? null,
                null
            ),
            new Supplier(
                $docHeader['Supplier']['CustomerAssignedAccountID'] ?? null,
                $docHeader['Supplier']['EndpointSchemeID'] ?? null,
                $docHeader['Supplier']['EndpointID'] ?? null,
                $docHeader['Supplier']['PartyIdentificationSchemeID'] ?? null,
                $docHeader['Supplier']['PartyIdentificationID'] ?? null,
                $docHeader['Supplier']['Name'] ?? null,
                $docHeader['Supplier']['StreetName'] ?? null,
                $docHeader['Supplier']['CityName'] ?? null,
                $docHeader['Supplier']['PostalZone'] ?? null,
                $docHeader['Supplier']['CountryCode'] ?? null,
                $docHeader['Supplier']['PartyTaxSchemeCompanyID'] ?? null,
                $docHeader['Supplier']['PartyLegalEntityCompanySchemeID'] ?? null,
                $docHeader['Supplier']['PartyLegalEntityCompanyID'] ?? null,
                $docHeader['Supplier']['ContactName'] ?? null,
                $docHeader['Supplier']['ContactTelephone'] ?? null,
                $docHeader['Supplier']['ContactElectronicMail'] ?? null
            ),
            new Customer(
                $docHeader['Customer']['EndpointSchemeID'] ?? null,
                $docHeader['Customer']['EndpointID'] ?? null,
                $docHeader['Customer']['PartyIdentificationSchemeID'] ?? null,
                $docHeader['Customer']['PartyIdentificationID'] ?? null,
                $docHeader['Customer']['Name'] ?? null,
                $docHeader['Customer']['StreetName'] ?? null,
                $docHeader['Customer']['CityName'] ?? null,
                $docHeader['Customer']['PostalZone'] ?? null,
                $docHeader['Customer']['CountryCode'] ?? null,
                $docHeader['Customer']['PartyTaxSchemeCompanyID'] ?? null,
                $docHeader['Customer']['PartyLegalEntityCompanySchemeID'] ?? null,
                $docHeader['Customer']['PartyLegalEntityCompanyID'] ?? null,
                $docHeader['Customer']['ContactName'] ?? null,
                $docHeader['Customer']['ContactTelephone'] ?? null,
                $docHeader['Customer']['ContactElectronicMail'] ?? null
            ),
            new Delivery(
                $docHeader['Delivery']['ActualDeliveryDate'] ?? null,
                $docHeader['Delivery']['StreetName'] ?? null,
                $docHeader['Delivery']['CityName'] ?? null,
                $docHeader['Delivery']['PostalZone'] ?? null,
                $docHeader['Delivery']['CountryCode'] ?? null
            ),
            $payments,
            $docHeader['Document']['PaymentTermsNote'] ?? null,
            $allowanceAndCharges,
            $docHeader['Tax']['TotalAmount'] ?? null,
            $taxSubtotals,
            new Totals(
                $docHeader['Totals']['LineExtensionAmount'] ?? null,
                $docHeader['Totals']['TaxExclusiveAmount'] ?? null,
                $docHeader['Totals']['TaxInclusiveAmount'] ?? null,
                $docHeader['Totals']['AllowanceTotalAmount'] ?? null,
                $docHeader['Totals']['ChargeTotalAmount'] ?? null,
                $docHeader['Totals']['PrepaidAmount'] ?? null,
                $docHeader['Totals']['PayableRoundingAmount'] ?? null,
                $docHeader['Totals']['PayableAmount'] ?? null
            ),
            $lines,
            $attachments
        );
    }

    private function getDocument23(string $json): Document
    {
        $jsonArray = $this->jsonToArray($json);
        $docHeader = $this->findOneInJson('DocumentHeader', $jsonArray);
        $docLines = $this->findOneInJson('DocumentLines', $jsonArray);
        $docAttachments = $this->findOneInJson('DocumentAttachments', $jsonArray);

        // Assemble attachments
        $attachments = array();
        foreach ($docAttachments as $docAttachment) {
            $attachments[] = new Attachment(
                $docAttachment['MimeCode'] ?? null,
                $docAttachment['Id'] ?? null,
                $docAttachment['Filename'] ?? null,
                $docAttachment['DocumentType'] ?? null,
                $docAttachment['Document'] ?? null
            );
        }

        // Assemble allowance and charges
        $allowanceAndCharges = array();
        foreach (($docHeader['AllowanceAndCharges'] ?? []) as $ac) {
            $allowanceAndCharges[] = new AllowanceAndCharge(
                $ac['ChargeIndicator'] ?? null,
                $ac['Amount'] ?? null,
                $ac['TaxCategory'] ?? null,
                $ac['TaxCategoryPercent'] ?? null,
                $ac['TaxScheme'] ?? null,
                $ac['AllowanceChargeReason'] ?? null
            );
        }

        // Assemble payments
        $payments = array();
        foreach (($docHeader['Payments'] ?? []) as $payment) {
            $payments[] = new Payment(
                $payment['Code'] ?? null,
                $payment['PaymentID'] ?? null,
                $payment['PayeeFinancialAccountID'] ?? null,
                $payment['PayeeFinancialAccountName'] ?? null,
                $payment['FinancialInstitutionBranchID'] ?? null,
                null
            );
        }

        // Assemble taxSubtotals
        $taxSubtotals = array();
        foreach (($docHeader['Tax']['SubTotals'] ?? []) as $taxSubTotal) {
            $taxSubtotals[] = new TaxSubtotal(
                $taxSubTotal['TaxableAmount'] ?? null,
                $taxSubTotal['TaxAmount'] ?? null,
                $taxSubTotal['TaxCategoryID'] ?? null,
                $taxSubTotal['TaxCategoryPercent'] ?? null,
                $taxSubTotal['TaxCategoryTaxExemptionReason'] ?? null
            );
        }

        // Assemble lines
        $lines = array();
        foreach ($docLines as $docLine) {
            // Assemble line allowance and charges
            $lineAllowanceAndCharges = array();
            foreach (($docLine['AllowanceAndCharges'] ?? []) as $ac) {
                $lineAllowanceAndCharges[] = new LineAllowanceAndCharge(
                    $ac['ChargeIndicator'] ?? null,
                    $ac['Amount'] ?? null,
                    $ac['ReasonCode'] ?? null,
                    $ac['AllowanceChargeReason'] ?? null,
                    $ac['MultiplierFactorNumeric'] ?? null,
                    $ac['BaseAmount'] ?? null
                );
            }

            $lines[] = new Line(
                $docLine['ID'] ?? null,
                $docLine['Note'] ?? null,
                $docLine['Quantity'] ?? null,
                $docLine['UnitCode'] ?? null,
                $docLine['LineExtensionAmount'] ?? null,
                $docLine['AccountingCost'] ?? null,
                $docLine['InvoicePeriodStartDate'] ?? null,
                $docLine['InvoicePeriodEndDate'] ?? null,
                $docLine['OrderLineReferenceLineID'] ?? null,
                $docLine['BuyersItemIdentificationID'] ?? null,
                $docLine['SellersItemIdentificationID'] ?? null,
                $docLine['StandardItemIdentificationID'] ?? null,
                $docLine['Description'] ?? null,
                $docLine['Name'] ?? null,
                $docLine['TaxCategoryID'] ?? null,
                $docLine['TaxCategoryPercent'] ?? null,
                $lineAllowanceAndCharges,
                $docLine['Price'] ?? null,
                $docLine['BaseQuantity'] ?? null,
                $docLine['BaseQuantityUnitCode'] ?? null,
                $docLine['PriceAllowanceChargeIndicator'] ?? null,
                $docLine['PriceAllowanceChargeAmount'] ?? null,
                $docLine['PriceAllowanceChargeBaseAmount'] ?? null,
            );
        }

        // Assemble document
        return new Document(
            $docHeader['Document']['DocNumber'] ?? null,
            $docHeader['Document']['DocType'] ?? null,
            $docHeader['Document']['DocFormat'] ?? null,
            $docHeader['Document']['IssueDate'] ?? null,
            $docHeader['Document']['DueDate'] ?? null,
            null,
            $docHeader['Document']['Note'] ?? null,
            $docHeader['Document']['CurrencyCode'] ?? null,
            new References(
                $docHeader['References']['AccountingCost'] ?? null,
                $docHeader['References']['BuyerReference'] ?? null,
                $docHeader['References']['InvoicePeriodStartDate'] ?? null,
                $docHeader['References']['InvoicePeriodEndDate'] ?? null,
                $docHeader['References']['OrderReference'] ?? null,
                $docHeader['References']['ContractDocumentReference'] ?? null,
                $docHeader['References']['ProjectReference'] ?? null,
                null
            ),
            new Supplier(
                $docHeader['Supplier']['CustomerAssignedAccountID'] ?? null,
                $docHeader['Supplier']['EndpointSchemeID'] ?? null,
                $docHeader['Supplier']['EndpointID'] ?? null,
                $docHeader['Supplier']['PartyIdentificationSchemeID'] ?? null,
                $docHeader['Supplier']['PartyIdentificationID'] ?? null,
                $docHeader['Supplier']['Name'] ?? null,
                $docHeader['Supplier']['StreetName'] ?? null,
                $docHeader['Supplier']['CityName'] ?? null,
                $docHeader['Supplier']['PostalZone'] ?? null,
                $docHeader['Supplier']['CountryCode'] ?? null,
                $docHeader['Supplier']['PartyTaxSchemeCompanyID'] ?? null,
                $docHeader['Supplier']['PartyLegalEntityCompanySchemeID'] ?? null,
                $docHeader['Supplier']['PartyLegalEntityCompanyID'] ?? null,
                $docHeader['Supplier']['ContactName'] ?? null,
                $docHeader['Supplier']['ContactTelephone'] ?? null,
                $docHeader['Supplier']['ContactElectronicMail'] ?? null
            ),
            new Customer(
                $docHeader['Customer']['EndpointSchemeID'] ?? null,
                $docHeader['Customer']['EndpointID'] ?? null,
                $docHeader['Customer']['PartyIdentificationSchemeID'] ?? null,
                $docHeader['Customer']['PartyIdentificationID'] ?? null,
                $docHeader['Customer']['Name'] ?? null,
                $docHeader['Customer']['StreetName'] ?? null,
                $docHeader['Customer']['CityName'] ?? null,
                $docHeader['Customer']['PostalZone'] ?? null,
                $docHeader['Customer']['CountryCode'] ?? null,
                $docHeader['Customer']['PartyTaxSchemeCompanyID'] ?? null,
                $docHeader['Customer']['PartyLegalEntityCompanySchemeID'] ?? null,
                $docHeader['Customer']['PartyLegalEntityCompanyID'] ?? null,
                $docHeader['Customer']['ContactName'] ?? null,
                $docHeader['Customer']['ContactTelephone'] ?? null,
                $docHeader['Customer']['ContactElectronicMail'] ?? null
            ),
            new Delivery(
                $docHeader['Delivery']['ActualDeliveryDate'] ?? null,
                $docHeader['Delivery']['StreetName'] ?? null,
                $docHeader['Delivery']['CityName'] ?? null,
                $docHeader['Delivery']['PostalZone'] ?? null,
                $docHeader['Delivery']['CountryCode'] ?? null
            ),
            $payments,
            $docHeader['Document']['PaymentTermsNote'] ?? null,
            $allowanceAndCharges,
            $docHeader['Tax']['TotalAmount'] ?? null,
            $taxSubtotals,
            new Totals(
                $docHeader['Totals']['LineExtensionAmount'] ?? null,
                $docHeader['Totals']['TaxExclusiveAmount'] ?? null,
                $docHeader['Totals']['TaxInclusiveAmount'] ?? null,
                $docHeader['Totals']['AllowanceTotalAmount'] ?? null,
                $docHeader['Totals']['ChargeTotalAmount'] ?? null,
                $docHeader['Totals']['PrepaidAmount'] ?? null,
                $docHeader['Totals']['PayableRoundingAmount'] ?? null,
                $docHeader['Totals']['PayableAmount'] ?? null
            ),
            $lines,
            $attachments
        );
    }

    private function getDocument24(string $json): Document
    {
        $jsonArray = $this->jsonToArray($json);
        $docHeader = $this->findOneInJson('DocumentHeader', $jsonArray);
        $docLines = $this->findOneInJson('DocumentLines', $jsonArray);
        $docAttachments = $this->findOneInJson('DocumentAttachments', $jsonArray);

        // Assemble attachments
        $attachments = array();
        foreach ($docAttachments as $docAttachment) {
            $attachments[] = new Attachment(
                $docAttachment['MimeCode'] ?? null,
                $docAttachment['Id'] ?? null,
                $docAttachment['Filename'] ?? null,
                $docAttachment['DocumentType'] ?? null,
                $docAttachment['Document'] ?? null
            );
        }

        // Assemble allowance and charges
        $allowanceAndCharges = array();
        foreach (($docHeader['AllowanceAndCharges'] ?? []) as $ac) {
            $allowanceAndCharges[] = new AllowanceAndCharge(
                $ac['ChargeIndicator'] ?? null,
                $ac['Amount'] ?? null,
                $ac['TaxCategory'] ?? null,
                $ac['TaxCategoryPercent'] ?? null,
                $ac['TaxScheme'] ?? null,
                $ac['AllowanceChargeReason'] ?? null
            );
        }

        // Assemble payments
        $payments = array();
        foreach (($docHeader['Payments'] ?? []) as $payment) {
            $payerFinancialAccount = isset($payment['PaymentMandatePayerFinancialAccountID']) ? 
                new PayerFinancialAccount($payment['PaymentMandatePayerFinancialAccountID']) : 
                null;
                
            $paymentMandateID = isset($payment['PaymentMandateID']) ? $payment['PaymentMandateID'] : null;

            $paymentMandate = ($paymentMandateID !== null || $payerFinancialAccount != null) ? 
                new PaymentMandate($paymentMandateID, $payerFinancialAccount) : 
                null;

            $payments[] = new Payment(
                $payment['PaymentMeansCode'] ?? $payment['Code'] ?? null,
                $payment['PaymentID'] ?? null,
                $payment['PayeeFinancialAccountID'] ?? null,
                $payment['PayeeFinancialAccountName'] ?? null,
                $payment['FinancialInstitutionBranchID'] ?? null,
                $paymentMandate
            );
        }

        // Assemble taxSubtotals
        $taxSubtotals = array();
        foreach (($docHeader['Tax']['SubTotals'] ?? []) as $taxSubTotal) {
            $taxSubtotals[] = new TaxSubtotal(
                $taxSubTotal['TaxableAmount'] ?? null,
                $taxSubTotal['TaxAmount'] ?? null,
                $taxSubTotal['TaxCategoryID'] ?? null,
                $taxSubTotal['TaxCategoryPercent'] ?? null,
                $taxSubTotal['TaxCategoryTaxExemptionReason'] ?? null
            );
        }

        // Assemble lines
        $lines = array();
        foreach ($docLines as $docLine) {
            // Assemble line allowance and charges
            $lineAllowanceAndCharges = array();
            foreach (($docLine['AllowanceAndCharges'] ?? []) as $ac) {
                $lineAllowanceAndCharges[] = new LineAllowanceAndCharge(
                    $ac['ChargeIndicator'] ?? null,
                    $ac['Amount'] ?? null,
                    $ac['ReasonCode'] ?? null,
                    $ac['AllowanceChargeReason'] ?? null,
                    $ac['MultiplierFactorNumeric'] ?? null,
                    $ac['BaseAmount'] ?? null
                );
            }

            $lines[] = new Line(
                $docLine['ID'] ?? null,
                $docLine['Note'] ?? null,
                $docLine['Quantity'] ?? null,
                $docLine['UnitCode'] ?? null,
                $docLine['LineExtensionAmount'] ?? null,
                $docLine['AccountingCost'] ?? null,
                $docLine['InvoicePeriodStartDate'] ?? null,
                $docLine['InvoicePeriodEndDate'] ?? null,
                $docLine['OrderLineReferenceLineID'] ?? null,
                $docLine['BuyersItemIdentificationID'] ?? null,
                $docLine['SellersItemIdentificationID'] ?? null,
                $docLine['StandardItemIdentificationID'] ?? null,
                $docLine['Description'] ?? null,
                $docLine['Name'] ?? null,
                $docLine['TaxCategoryID'] ?? null,
                $docLine['TaxCategoryPercent'] ?? null,
                $lineAllowanceAndCharges,
                $docLine['Price'] ?? null,
                $docLine['BaseQuantity'] ?? null,
                $docLine['BaseQuantityUnitCode'] ?? null,
                $docLine['PriceAllowanceChargeIndicator'] ?? null,
                $docLine['PriceAllowanceChargeAmount'] ?? null,
                $docLine['PriceAllowanceChargeBaseAmount'] ?? null,
            );
        }

        // Assemble document
        return new Document(
            $docHeader['Document']['DocNumber'] ?? null,
            $docHeader['Document']['DocType'] ?? null,
            $docHeader['Document']['DocFormat'] ?? null,
            $docHeader['Document']['IssueDate'] ?? null,
            $docHeader['Document']['DueDate'] ?? null,
            $docHeader['Document']['InvoiceTypeCode'] ?? null,
            $docHeader['Document']['Note'] ?? null,
            $docHeader['Document']['CurrencyCode'] ?? null,
            new References(
                $docHeader['References']['AccountingCost'] ?? null,
                $docHeader['References']['BuyerReference'] ?? null,
                $docHeader['References']['InvoicePeriodStartDate'] ?? null,
                $docHeader['References']['InvoicePeriodEndDate'] ?? null,
                $docHeader['References']['OrderReference'] ?? null,
                $docHeader['References']['ContractDocumentReference'] ?? null,
                $docHeader['References']['ProjectReference'] ?? null,
                $docHeader['References']['BillingReferenceID'] ?? null
            ),
            new Supplier(
                $docHeader['Supplier']['CustomerAssignedAccountID'] ?? null,
                $docHeader['Supplier']['EndpointSchemeID'] ?? null,
                $docHeader['Supplier']['EndpointID'] ?? null,
                $docHeader['Supplier']['PartyIdentificationSchemeID'] ?? null,
                $docHeader['Supplier']['PartyIdentificationID'] ?? null,
                $docHeader['Supplier']['Name'] ?? null,
                $docHeader['Supplier']['StreetName'] ?? null,
                $docHeader['Supplier']['CityName'] ?? null,
                $docHeader['Supplier']['PostalZone'] ?? null,
                $docHeader['Supplier']['CountryCode'] ?? null,
                $docHeader['Supplier']['PartyTaxSchemeCompanyID'] ?? null,
                $docHeader['Supplier']['PartyLegalEntityCompanySchemeID'] ?? null,
                $docHeader['Supplier']['PartyLegalEntityCompanyID'] ?? null,
                $docHeader['Supplier']['ContactName'] ?? null,
                $docHeader['Supplier']['ContactTelephone'] ?? null,
                $docHeader['Supplier']['ContactElectronicMail'] ?? null
            ),
            new Customer(
                $docHeader['Customer']['EndpointSchemeID'] ?? null,
                $docHeader['Customer']['EndpointID'] ?? null,
                $docHeader['Customer']['PartyIdentificationSchemeID'] ?? null,
                $docHeader['Customer']['PartyIdentificationID'] ?? null,
                $docHeader['Customer']['Name'] ?? null,
                $docHeader['Customer']['StreetName'] ?? null,
                $docHeader['Customer']['CityName'] ?? null,
                $docHeader['Customer']['PostalZone'] ?? null,
                $docHeader['Customer']['CountryCode'] ?? null,
                $docHeader['Customer']['PartyTaxSchemeCompanyID'] ?? null,
                $docHeader['Customer']['PartyLegalEntityCompanySchemeID'] ?? null,
                $docHeader['Customer']['PartyLegalEntityCompanyID'] ?? null,
                $docHeader['Customer']['ContactName'] ?? null,
                $docHeader['Customer']['ContactTelephone'] ?? null,
                $docHeader['Customer']['ContactElectronicMail'] ?? null
            ),
            new Delivery(
                $docHeader['Delivery']['ActualDeliveryDate'] ?? null,
                $docHeader['Delivery']['StreetName'] ?? null,
                $docHeader['Delivery']['CityName'] ?? null,
                $docHeader['Delivery']['PostalZone'] ?? null,
                $docHeader['Delivery']['CountryCode'] ?? null
            ),
            $payments,
            $docHeader['Document']['PaymentTermsNote'] ?? null,
            $allowanceAndCharges,
            $docHeader['Tax']['TotalAmount'] ?? null,
            $taxSubtotals,
            new Totals(
                $docHeader['Totals']['LineExtensionAmount'] ?? null,
                $docHeader['Totals']['TaxExclusiveAmount'] ?? null,
                $docHeader['Totals']['TaxInclusiveAmount'] ?? null,
                $docHeader['Totals']['AllowanceTotalAmount'] ?? null,
                $docHeader['Totals']['ChargeTotalAmount'] ?? null,
                $docHeader['Totals']['PrepaidAmount'] ?? null,
                $docHeader['Totals']['PayableRoundingAmount'] ?? null,
                $docHeader['Totals']['PayableAmount'] ?? null
            ),
            $lines,
            $attachments
        );
    }

    private function findInJson(string $type, array $jsonArray): array
    {
        $result = array();
        foreach ($jsonArray as $key => $value) {
            if($type === $key) {
                $result[] = $value;
            }
        }
        return $result;
    }

    private function findOneInJson(string $type, array $jsonArray, $default = array())
    {
        $result = $this->findInJson($type, $jsonArray);
        if(is_array($result) && !empty($result)) {
            $value = $result[array_key_first($result)];
            if(gettype($value) === gettype($default)) {
                return $value;
            }
        }
        return $default;
    }

    private function jsonToArray(string $data): array
    {
        if($jsonArray = json_decode($data, true)) {
            return $jsonArray;
        } else {
            return array();
        }
    }

    private function findMetadata(string $json): ?array
    {
        $jsonArray = $this->jsonToArray($json);
        return $this->findOneInJson('Metadata', $jsonArray);
    }

    private function supports(?array $metadata): bool
    {
        $versions = self::versions;

        if(is_null($metadata)) {
            return false;
        }

        if(!in_array($metadata['Version'], $versions)) {
            return false;
        }

        return true;
    }
}