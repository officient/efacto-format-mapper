<?php

namespace Officient\EfactoMapper;

use DOMDocument;
use Officient\EfactoMapper\Model\Document;

abstract class AbstractExporter
{
    public abstract function export(Document $document): DOMDocument;

    protected function initDOMDocument(): DOMDocument
    {
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        return $dom;
    }

    protected function xmlEscape($string): array|string
    {
        if(is_null($string)) {
            return '';
        }

        $string =  str_replace(array('&', '<', '>', '\'', '"'), array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;'), $string);
        return preg_replace('/[\x00-\x1F\x7F]/u', '', $string);
    }
}