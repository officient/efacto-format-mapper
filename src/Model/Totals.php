<?php

namespace Officient\EfactoMapper\Model;

class Totals
{
    /** @var string|null */
    protected ?string $lineExtensionAmount;
    /** @var string|null */
    protected ?string $taxExclusiveAmount;
    /** @var string|null */
    protected ?string $taxInclusiveAmount;
    /** @var string|null */
    protected ?string $allowanceTotalAmount;
    /** @var string|null */
    protected ?string $chargeTotalAmount;
    /** @var string|null */
    protected ?string $prepaidAmount;
    /** @var string|null */
    protected ?string $payableRoundingAmount;
    /** @var string|null */
    protected ?string $payableAmount;

    /**
     * @param string|null $lineExtensionAmount
     * @param string|null $taxExclusiveAmount
     * @param string|null $taxInclusiveAmount
     * @param string|null $allowanceTotalAmount
     * @param string|null $chargeTotalAmount
     * @param string|null $prepaidAmount
     * @param string|null $payableRoundingAmount
     * @param string|null $payableAmount
     */
    public function __construct(?string $lineExtensionAmount, ?string $taxExclusiveAmount, ?string $taxInclusiveAmount, ?string $allowanceTotalAmount, ?string $chargeTotalAmount, ?string $prepaidAmount, ?string $payableRoundingAmount, ?string $payableAmount)
    {
        $this->lineExtensionAmount = $lineExtensionAmount;
        $this->taxExclusiveAmount = $taxExclusiveAmount;
        $this->taxInclusiveAmount = $taxInclusiveAmount;
        $this->allowanceTotalAmount = $allowanceTotalAmount;
        $this->chargeTotalAmount = $chargeTotalAmount;
        $this->prepaidAmount = $prepaidAmount;
        $this->payableRoundingAmount = $payableRoundingAmount;
        $this->payableAmount = $payableAmount;
    }

    /**
     * @return string|null
     */
    public function getLineExtensionAmount(): ?string
    {
        return $this->lineExtensionAmount;
    }

    /**
     * @return string|null
     */
    public function getTaxExclusiveAmount(): ?string
    {
        return $this->taxExclusiveAmount;
    }

    /**
     * @return string|null
     */
    public function getTaxInclusiveAmount(): ?string
    {
        return $this->taxInclusiveAmount;
    }

    /**
     * @return string|null
     */
    public function getAllowanceTotalAmount(): ?string
    {
        return $this->allowanceTotalAmount;
    }

    /**
     * @return string|null
     */
    public function getChargeTotalAmount(): ?string
    {
        return $this->chargeTotalAmount;
    }

    /**
     * @return string|null
     */
    public function getPrepaidAmount(): ?string
    {
        return $this->prepaidAmount;
    }

    /**
     * @return string|null
     */
    public function getPayableRoundingAmount(): ?string
    {
        return $this->payableRoundingAmount;
    }

    /**
     * @return string|null
     */
    public function getPayableAmount(): ?string
    {
        return $this->payableAmount;
    }
}