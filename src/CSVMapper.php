<?php

namespace Officient\EfactoMapper;

use DOMDocument;
use Officient\EfactoMapper\Exception\UnsupportedFormatException;
use Officient\EfactoMapper\Exception\UnsupportedVersionException;
use Officient\EfactoMapper\Model\Attachment;
use Officient\EfactoMapper\Model\AllowanceAndCharge;
use Officient\EfactoMapper\Model\Customer;
use Officient\EfactoMapper\Model\Delivery;
use Officient\EfactoMapper\Model\Document;
use Officient\EfactoMapper\Model\Line;
use Officient\EfactoMapper\Model\LineAllowanceAndCharge;
use Officient\EfactoMapper\Model\Payment;
use Officient\EfactoMapper\Model\References;
use Officient\EfactoMapper\Model\Supplier;
use Officient\EfactoMapper\Model\TaxSubtotal;
use Officient\EfactoMapper\Model\Totals;

class CSVMapper implements MapperInterface
{
    const separators = [
        'semicolon' => ';',
        'tabulator' => '\t'
    ];

    const endTags = ['EOF'];

    public function mapToXRechnung(string $data): DOMDocument
    {
        // Verify support and get meta data
        $metadata = $this->findMetadata($data);
        if(!$this->supports($metadata)) {
            throw new UnsupportedFormatException();
        }

        // Build document object
        $document = $this->getDocument($data, $metadata);

        return (new XRechnungExporter())->export($document);
    }

    public function mapToXRechnungString(string $data): string
    {
        return $this->mapToXRechnung($data)->saveXML();
    }

    public function mapToPeppol3(string $data): DOMDocument
    {
        // Verify support and get meta data
        $metadata = $this->findMetadata($data);
        if(!$this->supports($metadata)) {
            throw new UnsupportedFormatException();
        }

        // Build document object
        $document = $this->getDocument($data, $metadata);

        return (new Peppol3Exporter())->export($document);
    }

    public function mapToPeppol3String(string $data): string
    {
        return $this->mapToPeppol3($data)->saveXML();
    }

    private function getDocument(string $data, array $metadata)
    {
        switch ($metadata['Version']) {
            case '2.2':
                return $this->getDocument22($data, self::separators[$metadata['separator']]);
            case '2.3':
                return $this->getDocument23($data, self::separators[$metadata['separator']]);
            default:
                throw new UnsupportedVersionException();
        }
    }

    /**
     * Get document from efacto csv version 2.2
     * @param string $csv
     * @param string $separator
     * @return Document
     */
    private function getDocument22(string $csv, string $separator): Document
    {
        $csvArray = $this->csvToArray($csv, $separator);
        $csvHeader = $this->findOneInCsvArray('DH', $csvArray);
        $csvLines = $this->findInCsvArray('DL', $csvArray);
        $csvAttachments = $this->findInCsvArray('DZ', $csvArray);

        // Assemble attachments
        $attachments = array();
        foreach ($csvAttachments as $csvAttachment) {
            $attachments[] = new Attachment(
                $csvAttachment[1] ?? null,
                $csvAttachment[2] ?? null,
                $csvAttachment[3] ?? null,
                $csvAttachment[4] ?? null,
                $csvAttachment[5] ?? null
            );
        }

        // Assemble allowance and charges
        $allowanceAndCharges = array();
        $acChargeIndicators = !empty($csvHeader[56]) ? explode(',', $csvHeader[56]) : [];
        $acAmounts = !empty($csvHeader[57]) ? explode(',', $csvHeader[57]) : [];
        $acTaxCategories = !empty($csvHeader[58]) ? explode(',', $csvHeader[58]) : [];
        $acTaxScheme = !empty($csvHeader[59]) ? explode(',', $csvHeader[59]) : [];
        foreach ($acChargeIndicators as $key => $value) {
            $allowanceAndCharges[] = new AllowanceAndCharge(
                $acChargeIndicators[$key] ?? null,
                $acAmounts[$key] ?? null,
                $acTaxCategories[$key] ?? null,
                $acTaxScheme[$key] ?? null,
                null,
                null
            );
        }

        // Assemble payments
        $payments = array();
        $paymentIds = !empty($csvHeader[51]) ? explode(',', $csvHeader[51]) : [];
        $payeeFinancialAccountIds = !empty($csvHeader[52]) ? explode(',', $csvHeader[52]) : [];
        $payeeFinancialAccountNames = !empty($csvHeader[53]) ? explode(',', $csvHeader[53]): [];
        $financialInstitutionBranchIds = !empty($csvHeader[54]) ? explode(',', $csvHeader[54]) : [];
        foreach ($paymentIds as $key => $value) {
            $payments[] = new Payment(
                '31',
                $paymentIds[$key] ?? null,
                $payeeFinancialAccountIds[$key] ?? null,
                $payeeFinancialAccountNames[$key] ?? null,
                $financialInstitutionBranchIds[$key] ?? null,
                null
            );
        }

        // Assemble taxSubtotals
        $taxSubtotals = array();
        $taxableAmounts = !empty($csvHeader[61]) ? explode(',', $csvHeader[61]): [];
        $taxAmounts = !empty($csvHeader[62]) ? explode(',', $csvHeader[62]) : [];
        $taxCategoryIds = !empty($csvHeader[63]) ? explode(',', $csvHeader[63]) : [];
        $taxCategoryPercents = !empty($csvHeader[64]) ? explode(',', $csvHeader[64]) : [];
        $taxCategoryTaxExemptionReasons = !empty($csvHeader[65]) ? explode(',', $csvHeader[65]) : [];
        foreach ($taxableAmounts as $key => $value) {
            $taxSubtotals[] = new TaxSubtotal(
                $taxableAmounts[$key] ?? null,
                $taxAmounts[$key] ?? null,
                $taxCategoryIds[$key] ?? null,
                $taxCategoryPercents[$key] ?? null,
                $taxCategoryTaxExemptionReasons[$key] ?? null
            );
        }

        // Assemble lines
        $lines = array();
        foreach ($csvLines as $csvLine) {
            // Assemble line allowance and charges
            $lineAllowanceAndCharges = array();
            $acIndicators = !empty($csvLine[17]) ? explode(',', $csvLine[17]) : [];
            $acAmounts = !empty($csvLine[18]) ? explode(',', $csvLine[18]) : [];
            $acReasonCodes = !empty($csvLine[19]) ? explode(',', $csvLine[19]) : [];
            foreach ($acIndicators as $key => $value) {
                $lineAllowanceAndCharges[] = new LineAllowanceAndCharge(
                    $acIndicators[$key] ?? null,
                    $acAmounts[$key] ?? null,
                    $acReasonCodes[$key] ?? null,
                    null
                );
            }

            $lines[] = new Line(
                $csvLine[1] ?? null,
                $csvLine[2] ?? null,
                $csvLine[3] ?? null,
                $csvLine[4] ?? null,
                $csvLine[5] ?? null,
                $csvLine[6] ?? null,
                $csvLine[7] ?? null,
                $csvLine[8] ?? null,
                $csvLine[9] ?? null,
                $csvLine[10] ?? null,
                $csvLine[11] ?? null,
                $csvLine[12] ?? null,
                $csvLine[13] ?? null,
                $csvLine[14] ?? null,
                $csvLine[15] ?? null,
                $csvLine[16] ?? null,
                $lineAllowanceAndCharges,
                $csvLine[20] ?? null,
                $csvLine[21] ?? null,
                $csvLine[22] ?? null,
                null,
                null,
                null
            );
        }

        /*$ud = array();
        for($i = 1; $i <= count($csvHeader); $i++) {
            $ud[$i."(".($i - 1).")"] = $csvHeader[($i - 1)];
        }
        dd($ud, count($ud));*/

        // Assemble document
        return new Document(
            $csvHeader[1] ?? null,
            $csvHeader[2] ?? null,
            $csvHeader[3] ?? null,
            $csvHeader[4] ?? null,
            $csvHeader[5] ?? null,
            null,
            $csvHeader[6] ?? null,
            $csvHeader[7] ?? null,
            new References(
                $csvHeader[8] ?? null,
                $csvHeader[9] ?? null,
                $csvHeader[10] ?? null,
                $csvHeader[11] ?? null,
                $csvHeader[12] ?? null,
                $csvHeader[13] ?? null,
                $csvHeader[14] ?? null
            ),
            new Supplier(
                $csvHeader[15] ?? null,
                $csvHeader[16] ?? null,
                $csvHeader[17] ?? null,
                $csvHeader[18] ?? null,
                $csvHeader[19] ?? null,
                $csvHeader[20] ?? null,
                $csvHeader[21] ?? null,
                $csvHeader[22] ?? null,
                $csvHeader[23] ?? null,
                $csvHeader[24] ?? null,
                $csvHeader[25] ?? null,
                $csvHeader[26] ?? null,
                $csvHeader[27] ?? null,
                $csvHeader[28] ?? null,
                $csvHeader[29] ?? null,
                $csvHeader[30] ?? null
            ),
            new Customer(
                $csvHeader[31] ?? null,
                $csvHeader[32] ?? null,
                $csvHeader[33] ?? null,
                $csvHeader[34] ?? null,
                $csvHeader[35] ?? null,
                $csvHeader[36] ?? null,
                $csvHeader[37] ?? null,
                $csvHeader[38] ?? null,
                $csvHeader[39] ?? null,
                $csvHeader[40] ?? null,
                $csvHeader[41] ?? null,
                $csvHeader[42] ?? null,
                $csvHeader[43] ?? null,
                $csvHeader[44] ?? null,
                $csvHeader[45] ?? null
            ),
            new Delivery(
                $csvHeader[46] ?? null,
                $csvHeader[47] ?? null,
                $csvHeader[48] ?? null,
                $csvHeader[49] ?? null,
                $csvHeader[50] ?? null
            ),
            $payments,
            $csvHeader[55] ?? null,
            $allowanceAndCharges,
            $csvHeader[60] ?? null,
            $taxSubtotals,
            new Totals(
                $csvHeader[66] ?? null,
                $csvHeader[67] ?? null,
                $csvHeader[68] ?? null,
                $csvHeader[69] ?? null,
                $csvHeader[70] ?? null,
                $csvHeader[71] ?? null,
                $csvHeader[72] ?? null,
                $csvHeader[73] ?? null
            ),
            $lines,
            $attachments
        );
    }

    /**
     * Get document from efacto csv version 2.3
     * @param string $csv
     * @param string $separator
     * @return Document
     */
    private function getDocument23(string $csv, string $separator): Document
    {
        $csvArray = $this->csvToArray($csv, $separator);
        $csvHeader = $this->findOneInCsvArray('DH', $csvArray);
        $csvLines = $this->findInCsvArray('DL', $csvArray);
        $csvAttachments = $this->findInCsvArray('DZ', $csvArray);

        // Assemble attachments
        $attachments = array();
        foreach ($csvAttachments as $csvAttachment) {
            $attachments[] = new Attachment(
                $csvAttachment[1] ?? null,
                $csvAttachment[2] ?? null,
                $csvAttachment[3] ?? null,
                $csvAttachment[4] ?? null,
                $csvAttachment[5] ?? null
            );
        }

        // Assemble allowance and charges
        $allowanceAndCharges = array();
        $acChargeIndicators = !empty($csvHeader[57]) ? explode(',', $csvHeader[57]) : [];
        $acAmounts = !empty($csvHeader[58]) ? explode(',', $csvHeader[58]) : [];
        $acTaxCategories = !empty($csvHeader[59]) ? explode(',', $csvHeader[59]) : [];
        $acTaxScheme = !empty($csvHeader[60]) ? explode(',', $csvHeader[60]) : [];
        foreach ($acChargeIndicators as $key => $value) {
            $allowanceAndCharges[] = new AllowanceAndCharge(
                $acChargeIndicators[$key] ?? null,
                $acAmounts[$key] ?? null,
                $acTaxCategories[$key] ?? null,
                $acTaxScheme[$key] ?? null,
                null,
                null
            );
        }

        // Assemble payments
        $payments = array();
        $code = !empty($csvHeader[51]) ? explode(',', $csvHeader[51]) : [];
        $paymentIds = !empty($csvHeader[52]) ? explode(',', $csvHeader[52]) : [];
        $payeeFinancialAccountIds = !empty($csvHeader[53]) ? explode(',', $csvHeader[53]) : [];
        $payeeFinancialAccountNames = !empty($csvHeader[54]) ? explode(',', $csvHeader[54]): [];
        $financialInstitutionBranchIds = !empty($csvHeader[55]) ? explode(',', $csvHeader[55]) : [];
        foreach ($paymentIds as $key => $value) {
            $payments[] = new Payment(
                $code[$key] ?? null,
                $paymentIds[$key] ?? null,
                $payeeFinancialAccountIds[$key] ?? null,
                $payeeFinancialAccountNames[$key] ?? null,
                $financialInstitutionBranchIds[$key] ?? null,
                null
            );
        }

        // Assemble taxSubtotals
        $taxSubtotals = array();
        $taxableAmounts = !empty($csvHeader[62]) ? explode(',', $csvHeader[62]): [];
        $taxAmounts = !empty($csvHeader[63]) ? explode(',', $csvHeader[63]) : [];
        $taxCategoryIds = !empty($csvHeader[64]) ? explode(',', $csvHeader[64]) : [];
        $taxCategoryPercents = !empty($csvHeader[65]) ? explode(',', $csvHeader[65]) : [];
        $taxCategoryTaxExemptionReasons = !empty($csvHeader[66]) ? explode(',', $csvHeader[66]) : [];
        foreach ($taxableAmounts as $key => $value) {
            $taxSubtotals[] = new TaxSubtotal(
                $taxableAmounts[$key] ?? null,
                $taxAmounts[$key] ?? null,
                $taxCategoryIds[$key] ?? null,
                $taxCategoryPercents[$key] ?? null,
                $taxCategoryTaxExemptionReasons[$key] ?? null
            );
        }

        // Assemble lines
        $lines = array();
        foreach ($csvLines as $csvLine) {
            // Assemble line allowance and charges
            $lineAllowanceAndCharges = array();
            $acIndicators = !empty($csvLine[17]) ? explode(',', $csvLine[17]) : [];
            $acAmounts = !empty($csvLine[18]) ? explode(',', $csvLine[18]) : [];
            $acReasonCodes = !empty($csvLine[19]) ? explode(',', $csvLine[19]) : [];
            foreach ($acIndicators as $key => $value) {
                $lineAllowanceAndCharges[] = new LineAllowanceAndCharge(
                    $acIndicators[$key] ?? null,
                    $acAmounts[$key] ?? null,
                    $acReasonCodes[$key] ?? null,
                    null
                );
            }

            $lines[] = new Line(
                $csvLine[1] ?? null,
                $csvLine[2] ?? null,
                $csvLine[3] ?? null,
                $csvLine[4] ?? null,
                $csvLine[5] ?? null,
                $csvLine[6] ?? null,
                $csvLine[7] ?? null,
                $csvLine[8] ?? null,
                $csvLine[9] ?? null,
                $csvLine[10] ?? null,
                $csvLine[11] ?? null,
                $csvLine[12] ?? null,
                $csvLine[13] ?? null,
                $csvLine[14] ?? null,
                $csvLine[15] ?? null,
                $csvLine[16] ?? null,
                $lineAllowanceAndCharges,
                $csvLine[20] ?? null,
                $csvLine[21] ?? null,
                $csvLine[22] ?? null,
                null,
                null,
                null
            );
        }

        /*$ud = array();
        for($i = 1; $i <= count($csvHeader); $i++) {
            $ud[$i."(".($i - 1).")"] = $csvHeader[($i - 1)];
        }
        dd($ud, count($ud));*/

        // Assemble document
        return new Document(
            $csvHeader[1] ?? null,
            $csvHeader[2] ?? null,
            $csvHeader[3] ?? null,
            $csvHeader[4] ?? null,
            $csvHeader[5] ?? null,
            null,
            $csvHeader[6] ?? null,
            $csvHeader[7] ?? null,
            new References(
                $csvHeader[8] ?? null,
                $csvHeader[9] ?? null,
                $csvHeader[10] ?? null,
                $csvHeader[11] ?? null,
                $csvHeader[12] ?? null,
                $csvHeader[13] ?? null,
                $csvHeader[14] ?? null
            ),
            new Supplier(
                $csvHeader[15] ?? null,
                $csvHeader[16] ?? null,
                $csvHeader[17] ?? null,
                $csvHeader[18] ?? null,
                $csvHeader[19] ?? null,
                $csvHeader[20] ?? null,
                $csvHeader[21] ?? null,
                $csvHeader[22] ?? null,
                $csvHeader[23] ?? null,
                $csvHeader[24] ?? null,
                $csvHeader[25] ?? null,
                $csvHeader[26] ?? null,
                $csvHeader[27] ?? null,
                $csvHeader[28] ?? null,
                $csvHeader[29] ?? null,
                $csvHeader[30] ?? null
            ),
            new Customer(
                $csvHeader[31] ?? null,
                $csvHeader[32] ?? null,
                $csvHeader[33] ?? null,
                $csvHeader[34] ?? null,
                $csvHeader[35] ?? null,
                $csvHeader[36] ?? null,
                $csvHeader[37] ?? null,
                $csvHeader[38] ?? null,
                $csvHeader[39] ?? null,
                $csvHeader[40] ?? null,
                $csvHeader[41] ?? null,
                $csvHeader[42] ?? null,
                $csvHeader[43] ?? null,
                $csvHeader[44] ?? null,
                $csvHeader[45] ?? null
            ),
            new Delivery(
                $csvHeader[46] ?? null,
                $csvHeader[47] ?? null,
                $csvHeader[48] ?? null,
                $csvHeader[49] ?? null,
                $csvHeader[50] ?? null
            ),
            $payments,
            $csvHeader[56] ?? null,
            $allowanceAndCharges,
            $csvHeader[61] ?? null,
            $taxSubtotals,
            new Totals(
                $csvHeader[67] ?? null,
                $csvHeader[68] ?? null,
                $csvHeader[69] ?? null,
                $csvHeader[70] ?? null,
                $csvHeader[71] ?? null,
                $csvHeader[72] ?? null,
                $csvHeader[73] ?? null,
                $csvHeader[74] ?? null
            ),
            $lines,
            $attachments
        );
    }

    private function csvToArray(string $csv, string $separator): array
    {
        $handle = fopen("php://temp", 'r+');
        fputs($handle, $csv);
        rewind($handle);

        $array = array();
        if($handle) {
            while(($line = fgetcsv($handle, 0, $separator)))
            {
                $array[] = $line;
            }
        }

        return $array;
    }

    private function findMetadata(string $csv): ?array
    {
        $handle = fopen("php://temp", 'r+');
        fputs($handle, $csv);
        rewind($handle);

        if($handle) {
            while(($line = fgets($handle)) !== false) {
                $exploded = explode(";", $line);
                if($exploded[0] === "MD") {
                    return [
                        'version' => trim($exploded[1]),
                        'separator' => trim($exploded[2]),
                        'endtag' => trim($exploded[3])
                    ];
                }
            }
        }
        return null;
    }

    private function supports(?array $metadata): bool
    {
        $versions = self::versions;
        $separators = array_keys(self::separators);
        $endTags = self::endTags;

        if(is_null($metadata)) {
            return false;
        }

        if(!in_array($metadata['version'], $versions)) {
            return false;
        }

        if(!in_array($metadata['separator'], $separators)) {
            return false;
        }

        if(!in_array($metadata['endtag'], $endTags)) {
            return false;
        }

        return true;
    }

    private function findInCsvArray(string $type, array $csvArray): array
    {
        $result = array();
        foreach ($csvArray as $row) {
            if(($row[0] ?? null) === $type) {
                $result[] = $row;
            }
        }
        return $result;
    }

    private function findOneInCsvArray(string $type, array $csvArray): ?array
    {
        $result = $this->findInCsvArray($type, $csvArray);
        return !empty($result) ? $result[array_key_first($result)] : null;
    }
}