<?php

namespace Officient\EfactoMapper\Model;

class LineAllowanceAndCharge
{
    /** @var string|null */
    protected ?string $acIndicator;
    /** @var string|null */
    protected ?string $acAmount;
    /** @var string|null */
    protected ?string $acReasonCode;
    /** @var string|null */
    protected ?string $acAllowanceChargeReason;
    /** @var string|null */
    protected ?string $acMultiplierFactorNumeric;
    /** @var string|null */
    protected ?string $acBaseAmount;

    /**
     * @param string|null $acIndicator
     * @param string|null $acAmount
     * @param string|null $acReasonCode
     * @param string|null $acAllowanceChargeReason
     * @param string|null $acMultiplierFactorNumeric
     */
    public function __construct(
        ?string $acIndicator,
        ?string $acAmount,
        ?string $acReasonCode,
        ?string $acAllowanceChargeReason,
        ?string $acMultiplierFactorNumeric,
        ?string $acBaseAmount
    )
    {
        $this->acIndicator = $acIndicator;
        $this->acAmount = $acAmount;
        $this->acReasonCode = $acReasonCode;
        $this->acAllowanceChargeReason = $acAllowanceChargeReason;
        $this->acMultiplierFactorNumeric = $acMultiplierFactorNumeric;
        $this->acBaseAmount = $acBaseAmount;
    }

    /**
     * @return string|null
     */
    public function getAcIndicator(): ?string
    {
        return $this->acIndicator;
    }

    /**
     * @return string|null
     */
    public function getAcAmount(): ?string
    {
        return $this->acAmount;
    }

    /**
     * @return string|null
     */
    public function getAcReasonCode(): ?string
    {
        return $this->acReasonCode;
    }

    /**
     * @return string|null
     */
    public function getAcAllowanceChargeReason(): ?string
    {
        return $this->acAllowanceChargeReason;
    }

    /**
     * @return string|null
     */
    public function getAcMultiplierFactorNumeric(): ?string
    {
        return $this->acMultiplierFactorNumeric;
    }

    /**
     * @return string|null
     */
    public function getAcBaseAmount(): ?string
    {
        return $this->acBaseAmount;
    }
}